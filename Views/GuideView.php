<?php
/**
 * @brief     View for the Guide page.
 * @ingroup   guide_views
 * @file      GuideView.php
 * @namespace Ritc/Guide/Views
 * @author    William E Reveal <bill@revealitconsulting.com>
 * @version   1.0.0-alpha.1
 * @date      2015-12-17 14:39:27
 * @note <b>Change Log</b>
 * - v1.0.0-alpha.1 - Initial rewrite version                     - 12/17/2015 wer
 * - v1.0.0-alpha.0 - Original version                            - 03/07/2014 wer
 * @TODO Ritc/Guide/Views/GuideView.php - Pretty much everything.
 */
namespace Ritc\Guide\Views;

use Ritc\Guide\Traits\GuideViewTraits;
use Ritc\Library\Traits\LogitTraits;

/**
 * Class GuideView
 * @class   GuideView
 * @package Ritc\Guide\Views
 */
class GuideView
{
    use GuideViewTraits;

    /**
     * Renders the home page for the Guide.
     * @return string
     */
    public function renderHomePage()
    {
        return "<p>Hi!</p>";
    }
}
