<?php
/**
 * @brief     View for the Manager page.
 * @ingroup   guide_views
 * @file      ManagerView.php
 * @namespace Ritc\Guide\Views
 * @author    William Reveal  <bill@revealitconsulting.com>
 * @version   1.0.0-alpha.1
 * @date      2016-03-05 08:41:33
 * @note <b>Change Log</b>
 * - v1.0.0-alpha.1 - Initial rewrite        - 2016-03-05 wer
 * - v1.0.0-alpha.0 - Initial version        - 03/07/2014 wer
 */
namespace Ritc\Guide\Views;

use Ritc\Guide\Traits\GuideViewTraits;
use Ritc\Library\Traits\LogitTraits;

/**
 * Class ManagerView.
 * @class ManagerView
 * @package Ritc\Guide\Views
 */
class ManagerView
{
    use GuideViewTraits;

    /** @var int  */
    private $default_section = 1;
    /** @var array  */
    private $default_tpl_values;
    /** @var int  */
    private $num_to_display = 10;

    /**
     * ManagerView constructor.
     */
    public function __construct()
    {
        $this->default_tpl_values = $this->initializeTplValues();
        $this->default_section    = $this->getDefaultSection();
        $this->num_to_display     = $this->getNumToDisplay();
    }
}
