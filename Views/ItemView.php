<?php
/**
 * @brief     View for the Item page.
 * @ingroup   guide_views
 * @file      ItemView.php
 * @namespace Ritc\Guide\Views
 * @author    William E Reveal <bill@revealitconsulting.com>
 * @version   1.0.0-alpha.1
 * @date      2016-03-05 08:40:14
 * @note <b>Change Log</b>
 * - v1.0.0-alpha.1 - Initial rewrite        - 2016-03-05 wer
 * - v1.0.0-alpha.0 - Initial version        - 03/07/2014 wer
 * @todo Ritc/Guide/Views/ItemView.php - bugs need squashed
 */
namespace Ritc\Guide\Views;

use Ritc\Guide\Models\ItemModel;
use Ritc\Guide\Traits\GuideViewTraits;
use Ritc\Library\Helper\Arrays;
use Ritc\Library\Helper\Strings;
use Ritc\Library\Traits\LogitTraits;

/**
 * Class ItemView
 * @class     ItemView
 * @package Ritc\Guide\Views
 */
class ItemView
{
    use GuideViewTraits;

    /**
     * @var \Ritc\Guide\Models\ItemModel
     */
    private $o_item;
    /**
     * @var int
     */
    private $default_section = 1;
    /**
     * @var array
     */
    private $default_tpl_values;
    /**
     * @var int
     */
    private $num_to_display = 10;

    /**
     * ItemView constructor.
     */
    public function __construct()
    {
        $this->o_item             = new ItemModel();
        $this->default_tpl_values = $this->initializeTplValues();
        $this->default_section    = $this->getDefaultSection();
        $this->num_to_display     = $this->getNumToDisplay();
    }
    /**
     * Returns the html that displays a single item.
     * @param string $item_name
     * @return string
    **/
    public function renderItem($item_name = '')
    {
        if ($item_name == '') { return ''; }
        $a_item_values = $this->o_item->readFieldByName($item_name);
        $a_tpl_values = $this->default_tpl_values;
        $a_tpl_values['title'] = '{$item_name}';
        $a_tpl_values['description'] = substr($a_item_values['description'],0,140);
        $a_tpl_values['a_item'] = $a_item_values;
        return $this->o_twig->render('@pages/item.twig', $a_tpl_values);
    }
    /**
     * Renders the html for the vCard list
     * @param int $num_to_display
     * @return string
    **/
    public function renderVCards($num_to_display = 10)
    {
        $a_item_cards = $this->itemCards($num_to_display);
        $a_tpl_values = $this->default_tpl_values;
        $a_tpl_values['title'] = 'Home Page';
        $a_tpl_values['description'] = 'This is the home page for the guide';
        $a_tpl_values['item_cards'] = $a_item_cards;
        return $this->o_twig->render('@pages/index.twig', $a_tpl_values);
    }
    /**
     * creates the values to be used for the item cards
     * @param int $num_to_display defaults to -1
     * @return array $a_values
    **/
    public function itemCards($num_to_display = -1)
    {
        // look for featured items first.
        // if there are some, grab the first 10 and return them
        // else grab 10 random items
        if ($num_to_display == -1) {
           $num_to_display = $this->num_to_display;
        }
        else {
            $this->num_to_display = $num_to_display;
        }
        $a_items = $this->o_item->readItemFeatured($num_to_display);
        if ($a_items === false || count($a_items) <= 0) {
            $a_items = $this->o_item->readItemRandom($num_to_display);
        }
        elseif (count($a_items) < $num_to_display) {
            $a_more_items = $this->o_item->readItemRandom($num_to_display - count($a_items));
            if ($a_more_items !== false && count($a_more_items) > 0) {
                $a_items = array_merge($a_items, $a_more_items);
            }
        }
        $a_items = Arrays::removeSlashes($a_items);
        $this->o_elog->write('' . var_export($a_items, TRUE), LOG_OFF, __METHOD__ . '.' . __LINE__);
        $a_search_parameters = array(
            'search_type' => 'AND'
        );
        $a_search_for_fields = array(
            'about',
            'street',
            'city',
            'federal_state',
            'postcode',
            'phone'
        );
        $a_items = $this->o_views->addDataToItems($a_items, $a_search_for_fields, $a_search_parameters);
        foreach ($a_items as $key=>$a_item) {
            if (strlen($a_items[$key]['about']) > 0) {
                $a_items[$key]['about'] = Strings::makeShortString($a_items[$key]['about'], 12)
                    . '... <a href="/item/'
                    . $a_items[$key]['item_id']
                    . '/">More</a>';
            }
        }
        $this->o_elog->write('a_items: ' . var_export($a_items, true), LOG_OFF, __METHOD__ . '.' . __LINE__);
        return $a_items;
    }
}
