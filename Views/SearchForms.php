<?php
/**
 * @brief     Creates forms for the Guide.
 * @ingroup   guide_views
 * @file      SearchForms.php
 * @namespace Ritc\Guide\Views
 * @author    William E Reveal <bill@revealitconsulting.com>
 * @version   1.0.0-alpha.1
 * @date      2016-03-05 08:45:21
 * @note <b>Change Log</b>
 * - v1.0.0-alpha.1  - Initial rewrite        - 2016-03-05 wer
 * - v0.1            - Initial version        - 2012-06-13 wer
 */

namespace Ritc\Guide\Views;

use Ritc\Guide\Traits\GuideViewTraits;
use Ritc\Library\Traits\LogitTraits;

/**
 * Class SearchForms
 * @class     SearchForms
 * @package Ritc\Guide\Views
 */
class SearchForms
{
    use GuideViewTraits, LogitTraits;
    /**
     * Returns a rendered form.
     * @param array $a_form_values
     * @return object
     */
    public function quickSearch(array $a_form_values = array())
    {
        return $this->o_twig->render('@guide_snippets/quickform.twig', $a_form_values);
    }
}
