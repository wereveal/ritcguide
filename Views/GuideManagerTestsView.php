<?php
/**
 * @brief     Guide Tests Views creates the output for the app.
 * @details   Gets the values and converts them to html.
 * @ingroup   guide_views
 * @file      GuideManagerTestsView.php
 * @namespace Ritc\Guide\Views
 * @author    William E Reveal <bill@revealitconsulting.com>
 * @version   1.0.0-alpha.0
 * @date      2016-03-09 14:23:31
 * @note Change Log
 * - v1.0.0-alpha.0 - Initial version        - 2016-03-09 wer
 */
namespace Ritc\Guide\Views;

use Ritc\Guide\Tests\SectionModelTester;
use Ritc\Guide\Traits\GuideViewTraits;
use Ritc\Library\Services\Di;
use Ritc\Library\Traits\LogitTraits;

/**
 * Class GuideManagerTestsView.
 * @class   GuideManagerTestsView
 * @package Ritc\Guide\Views
 */
class GuideManagerTestsView
{
    use GuideViewTraits;

    /**
     * GuideManagerTestsView constructor.
     * @param \Ritc\Library\Services\Di $o_di
     */
    public function __construct(Di $o_di)
    {
        $this->setupView($o_di);
        if (defined('DEVELOPER_MODE') && DEVELOPER_MODE) {
            $this->o_elog  = $o_di->get('elog');
        }
    }

    /**
     * @return string
     */
    public function renderPage()
    {
        $a_values = $this->getPageValues();
        $navgroup_name = str_replace('View', '', __CLASS__);
        $this->setNavByNgName($navgroup_name);
        $a_values['links'] = $this->a_nav;
        return $this->o_twig->render('@guide_pages/manager_tests.twig', $a_values);
    }

    /**
     * Returns the html displaying the results of the tests.
     * @param string $route_action Can be 'all' for all tests
     *                             or individual tests e.g. 'model-section'
     * @return string
     */
    public function renderTest($url_action = 'all')
    {
        $meth = __METHOD__ . '.';
        $this->logIt("URL Action: " . $url_action, LOG_OFF, $meth . __LINE__);
        switch ($url_action) {
            case '':
            case 'all':
                $results = $this->renderAllTests();
                break;
            default:
                $name = 'run' . $url_action . 'Tests';
                $results =  $this->$name();
        }
        return var_export($results, true);
    }

    /**
     * @return array
     */
    public function renderAllTests()
    {
        $results = $this->runModelsSectionTests();
        return $results;
    }

    /**
     * @return array
     */
    protected function runModelsSectionTests()
    {
        $o_sec_model = new SectionModelTester($this->o_di);
        $o_sec_model->runTests();
        return $o_sec_model->returnTestResults(true);
    }
}
