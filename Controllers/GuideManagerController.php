<?php
/**
 *  @brief     Main Manager Controller for the Guide.
 *  @ingroup   guide_controllers
 *  @file      GuideManagerController.php
 *  @namespace Ritc\Guide\Controllers
 *  @author    William E Reveal <bill@revealitconsulting.com>
 *  @version   1.0.0-alpha.1
 *  @date      2016-03-05 08:32:52
 *  @note <pre><b>Change Log</b>
 *      v1.0.0-alpha.1 - rewrite time       - 2016-03-05 wer
 *      v0.3ß          - total makeover     - 09/25/2014 wer
 *      v0.2           - New repository     - 2013-03-26
 *      v0.1           - Initial version    - 2012-06-04
 *  </pre>
 *  @todo GuideManagerController.php - pretty much everything has to be rewritten because of changes to the Library
**/
namespace Ritc\Guide\Controllers;

use Ritc\Library\Interfaces\ControllerInterface;
use Ritc\Library\Services\Di;

/**
 * Class GuideManagerController.
 * @class   GuideManagerController
 * @package Ritc\Guide\Controllers
 */
class GuideManagerController implements ControllerInterface
{
    /** @var \Ritc\Library\Services\Session  */
    private $o_session;

    /**
     * GuideManagerController constructor.
     * @param \Ritc\Library\Services\Di $o_di
     */
    public function __construct(Di $o_di)
    {
        $this->o_session = $o_di->get('session');
    }

    /**
     * Main Pukerouter.
     * @return string
     */
    public function route()
    {
        switch ($a_actions['action2']) {
            case 'fix_fields':
                return $this->fixFieldsAction();
            case 'list_fields':
                return $this->listFieldsAction();
            case 'config_admin':
                $o_config_admin = new ConfigAdminController($this->o_di);
                if (!isset($a_actions['action3'])) {
                    $a_actions['action3'] = '';
                }
                return $o_config_admin->render();
            default:
                return $this->indexAction();
        }
    }

    /**
     *  Displays a list of actions that one can do in the manager
    **/
    public function indexAction()
    {
        $a_tpl_values = $this->a_tpl_values;
        $a_tpl_values['description'] = 'Main Manager Page';
        return $this->render('@guide_pages/manager.twig', $a_tpl_values);
    }

    /**
     *  Creates the values for the ritc_fields_sections table..
     *  @param none
     *  @return string html
    **/
    public function createFieldsSectionsAction()
    {
        $a_tpl_values = $this->a_tpl_values;
        $a_tpl_values['description'] = 'Fields Sections Fix';
        $a_tpl_values['title'] = 'Fields Sections Fix';
        $a_tpl_values['body_text'] = 'Fields Sections Fix';
        return $this->o_twig->render('@guide_manager/fields.twig', $a_tpl_values);
    }

    /**
     *  Displays a list of the fields
     *  @param none
     *  @return string html
    **/
    public function listFieldsAction()
    {
        $a_tpl_values = $this->a_tpl_values;
        $a_tpl_values['description'] = 'View Fields';
        $a_fields = $this->o_field->readField();
        $a_tpl_values['body_text'] = '';
        $a_tpl_values['fields'] = $a_fields;
        return $this->render('@manager/field.twig', $a_tpl_values);
    }
}
