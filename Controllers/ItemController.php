<?php
/**
 *  @brief     Controller for Item.
 *  @ingroup   guide_controllers
 *  @file      ItemController.php
 *  @namespace Ritc\Guide\Controllers
 *  @author    William E Reveal <bill@revealitconsulting.com>
 *  @version   1.0.0-alpha.1
 *  @date      2016-03-05 08:33:43
 *  @note <pre><b>Change Log</b>
 *      v1.0.0-alpha.1  - initial rewrite    - 2016-03-05 wer
 *      v0.1            - Initial version    - 06/03/2013 wer
 *  </pre>
 *  @todo Ritc/Guide/Controllers/ItemController.php - pretty much everything has to be rewritten.
 *       - Implement the GuideViewTraits trait
 *       - Fix the bugs
**/
namespace Ritc\Guide\Controllers;

use Ritc\Guide\Models\CategoryModel;
use Ritc\Guide\Models\FieldModel;
use Ritc\Guide\Models\ItemModel;
use Ritc\Guide\Models\SectionModel;
use Ritc\Library\Helper\Arrays;
use Ritc\Library\Helper\Strings;

/**
 * Class ItemController.
 * @class   ItemController
 * @package Ritc\Guide\Controllers
 */
class ItemController
{
    /** @var int  */
    protected $default_section = 1;
    /** @var int  */
    protected $num_to_display  = 10;
    /** @var string  */
    protected $phone_format    = "AAA-BBB-CCCC";
    /** @var string  */
    protected $date_format     = "mm/dd/YYYY";
    /** @var array  */
    protected $a_tpl_values;
    /** @var \Ritc\Library\Helper\Arrays  */
    protected $o_arr;
    /** @var \Ritc\Guide\Controllers\Category  */
    protected $o_cat;
    /**
     * @var
     */
    protected $o_elog;
    /** @var \Ritc\Guide\Models\ItemModel  */
    protected $o_item;
    /** @var \Ritc\Guide\Controllers\Section  */
    protected $o_sec;
    /** @var \Ritc\Library\Helper\Strings  */
    protected $o_str;
    /** @var \Ritc\Guide\Controllers\Twig_Environment  */
    protected $o_twig;
    /**
     * @var
     */
    protected $o_view;

    /**
     * ItemController constructor.
     */
    public function __construct()
    {
        $this->o_arr   = new Arrays();
        $this->o_cat   = new Category();
        $this->o_elog  = Elog::start();
        $this->o_field = new Field;
        $this->o_item  = new ItemModel;
        $this->o_sec   = new Section;
        $this->o_str   = new Strings;
        $loader        = $this->twigLoader();
        $this->o_twig  = new Twig_Environment(
            $loader,
            array(
                'cache'       => SRC_PATH . '/twig_cache',
                'debug'       => false,
                'auto_reload' => true,
                'autoescape'  => true
            )
        );
        if (defined(DISPLAY_DATE_FORMAT)) {
            $this->date_format = DISPLAY_DATE_FORMAT;
        }
        if (defined(DISPLAY_PHONE_FORMAT)) {
            $this->phone_format = DISPLAY_PHONE_FORMAT;
        }
        $this->initializeTplValues();
    }

    /**
     * @param array $a_actions
     * @param array $a_values
     */
    public function router(array $a_actions = array(), array $a_values = array())
    {

    }

    ### Main Actions called from routing ###
    /**
     *  Displays the result of a simple search (from the quick search form).
     *  @param int $item_id required, redirects to main page if missing
     *  @return str the html to display
     *  @todo ItemController::router()
     *  - Display all the information available
     *  - Write a function which figures out time open - see rcreader dining guide export
    **/
    protected function defaultAction($item_id = '')
    {
        if ($item_id == '') {
            header('Location: ' . SITE_URL);
        }
        $a_item = $this->o_item->readItem(array('item_id' => $item_id));
        $a_item = $this->o_arr->removeSlashes($a_item[0]);
        $a_item = $this->addDataToItem($a_item);
        $a_item_data = $a_item[0];
        $this->o_elog->write('Item: ' . var_export($a_item_data, true), LOG_OFF, __METHOD__ . '.' . __LINE__);
        $a_tpl_values = $this->a_tpl_values;
        $a_tpl_values['item_data'] = $a_item_data;
        $a_tpl_values['title'] .= ' - ' . $a_item_data['item_name'];
        return $this->o_twig->render('@pages/item.twig', $a_tpl_values);;
    }
    ### Other Methods ###
    /**
     *  Gets all the data for the item specified.
     *  This uses the base method addDataToItems which works for multiple items
     *  @param array $a_item required
     *  @return array $a_item
    **/
    public function addDataToItem($a_item = '')
    {
        $a_search_for_fields = array();
        $a_search_parameters = array(
            'order_by' => 'f.field_short_description'
        );
        $a_items = array();
        $a_items[] = $a_item;
        return $this->addDataToItems($a_items, $a_search_for_fields, $a_search_parameters);
    }
}
