<?php
/**
 *  @brief     The main Controller for the whole site.
 *  @ingroup   guide_controllers
 *  @file      MainController.php
 *  @namespace Ritc\Guide\Controllers
 *  @author    William E Reveal  <bill@revealitconsulting.com>
 *  @version   v1.0.0-alpha.1
 *  @date      2016-03-05 08:33:02
 *  @note <pre><b>Change Log</b>
 *      v1.0.0-alpha.1 - Initial rewrite        - 2016-03-05 wer
 *      v0.2.0         - Working on it          - 07/02/2013 wer
 *      v0.1.0         - Initial attempt        - 2012-06-04 wer
 *  </pre>
 *  @todo Ritc/Guide/Controllers/MainController.php - Pretty much everything has to be rewritten to match Library changes.
**/
namespace Ritc\Guide\Controllers;

use Ritc\Guide\Views\GuideView;
use Ritc\Library\Interfaces\PageControllerInterface;
use Ritc\Library\Services\Di;
use Ritc\Library\Services\Router;
use Ritc\Library\Services\Session;
use Ritc\Library\Traits\LogitTraits;

/**
 * Class MainController.
 * @class     MainController
 * @package Ritc\Guide\Controllers
 */
class MainController implements PageControllerInterface
{
    use LogitTraits;

    /** @var Di  */
    protected $o_di;
    /** @var Router  */
    protected $o_router;
    /** @var Session  */
    protected $o_session;
    /** @var GuideView  */
    protected $o_view;

    /**
     * MainController constructor.
     * @param Di $o_di
     */
    public function __construct(Di $o_di)
    {
        $this->o_di      = $o_di;
        $this->o_router  = $o_di->get('router');
        $this->o_session = $o_di->get('session');
        $this->o_view    = new GuideView;
        if (defined('DEVELOPER_MODE') && DEVELOPER_MODE) {
            $this->o_elog = $o_di->get('elog');
        }

    }
    /**
     *  Main Router and Puker outer.
     *  Turns over the hard work to the specific controllers.
     *  @param none
     *  @return string $html
    **/
    public function renderPage()
    {
        $a_route_parts = $this->o_router->getRouteParts();
        $this->logIt(var_export($a_route_parts, true), LOG_OFF, __METHOD__ . '.' . __LINE__);
        switch ($a_route_parts['route_class']) {
            case 'GuideManagerController':
                $o_controller = new GuideManagerController($this->o_di);
                break;
            case 'SearchController':
                $o_controller = new SearchController($this->o_di);
                break;
            case 'ItemController':
                $o_controller = new ItemController($this->o_di);
                break;
            case 'HomeController':
            default:
                $o_controller = new HomeController($this->o_di);
        }
        if ($a_route_parts['route_method'] == '') {
            $a_route_parts['route_method'] = 'render';
        }
        return $o_controller->$a_route_parts['route_method']();
    }
}
