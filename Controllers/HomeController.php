<?php
/**
 *  @brief     Controller for the home page of the Guide.
 *  @ingroup   guide_controllers
 *  @file      HomeController.php
 *  @namespace Ritc\Guide\Controllers
 *  @author    William E Reveal <bill@revealitconsulting.com>
 *  @version   1.0.0-alpha.1
 *  @date      2015-12-17 13:06:18
 *  @note <pre><b>Change Log</b>
 *      v1.0.0-alpha.1 - New initial version                                                            - 12/17/2015 wer
 *      v0.2           - New repository and name change                                                 - 2013-03-26 wer
 *      v0.1           - Initial version                                                                - 2012-06-04 wer
 *  </pre>
 *  @todo Ritc/Guide/Controllers/HomeController.php - pretty much everything.
**/
namespace Ritc\Guide\Controllers;

use Ritc\Guide\Views\GuideView;
use Ritc\Library\Interfaces\ControllerInterface;
use Ritc\Library\Services\Di;
use Ritc\Library\Traits\LogitTraits;

/**
 * Class HomeController
 * @class   HomeController
 * @package Ritc\Guide\Controllers
 */
class HomeController implements ControllerInterface
{
    use LogitTraits;

    /** @var array  */
    protected $a_tpl_values;
    /** @var \Ritc\Library\Services\Di  */
    protected $o_di;
    /** @var \Ritc\Library\Services\Router  */
    protected $o_router;
    /** @var \Ritc\Guide\Views\GuideView  */
    protected $o_view;

    /**
     * HomeController constructor.
     * @param Di $o_di
     */
    public function __construct(Di $o_di)
    {
        $this->o_di     = $o_di;
        $this->o_router = $o_di->get('router');
        $this->o_view   = new GuideView;
        $this->o_view->setupView($o_di);
        if (defined('DEVELOPER_MODE') && DEVELOPER_MODE) {
            $this->o_elog = $o_di->get('elog');
            $this->o_view->setElog($this->o_elog);
        }
    }
    /**
     * Generic render method required by interface.
     * @return string
     */
    public function route()
    {
        return $this->o_view->renderHomePage();
    }

}
