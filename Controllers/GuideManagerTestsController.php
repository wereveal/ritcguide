<?php
/**
* @brief     The test controller for the Guide..
* @details   The test controller for the Guide.
* @ingroup   guide_controllers
* @file      GuideManagerTestsController.php
* @namespace Ritc\Guide\Controllers
* @author    William E Reveal <bill@revealitconsulting.com>
* @version   1.0.0-alpha.0
* @date      2016-03-09 14:16:20
* @note Change Log
* - v1.0.0-alpha.0 - Initial version        - 2016-03-09 wer
* @todo Ritc/Guide/Controllers/GuideManagerTestsController.php - everything
*/
namespace Ritc\Guide\Controllers;

use Ritc\Guide\Views\GuideManagerTestsView;
use Ritc\Library\Interfaces\ControllerInterface;
use Ritc\Library\Services\Di;
use Ritc\Library\Services\Router;
use Ritc\Library\Traits\LogitTraits;

/**
* Class GuideManagerTestsController.
* @class   GuideManagerTestsController
* @package Ritc\Guide\Controllers
*/
class GuideManagerTestsController implements ControllerInterface
{
    use LogitTraits;

    /** @var \Ritc\Library\Services\Di  */
    protected $o_di;
    /** @var \Ritc\Library\Services\Router */
    protected $o_router;

    public function __construct(Di $o_di)
    {
        $this->o_di = $o_di;
        $this->o_router = $this->o_di->get('router');
        if (defined('DEVELOPER_MODE') && DEVELOPER_MODE) {
            $this->o_elog  = $o_di->get('elog');
        }
    }

    public function route()
    {
        $meth = __METHOD__ . '.';
        $o_view   = new GuideManagerTestsView($this->o_di);
        $a_route_parts = $this->o_router->getRouteParts();
        $a_url_actions = $a_route_parts['url_actions'];

        $log_message = 'Route Parts ' . var_export($a_route_parts, TRUE);
        $this->logIt($log_message, LOG_OFF, $meth . __LINE__);
        $log_message = 'url actions ' . var_export($a_url_actions, TRUE);
        $this->logIt($log_message, LOG_OFF, $meth . __LINE__);

        $url_action = '';
        foreach($a_url_actions as $action) {
            $url_action .= ucfirst($action);
        }
        $this->logIt("url_action: " . $url_action, LOG_OFF, $meth . __LINE__);
        if ($url_action != '') {
            return $o_view->renderTest($url_action);
        }
        else {
            return $o_view->renderPage();
        }
    }
}
