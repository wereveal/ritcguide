<?php
/**
 * @brief     Tests the Category Model Class.
 * @ingroup   guide_tests
 * @file      CategoryTester.php
 * @namespace Ritc\Guide\Tests
 * @author    William E Reveal <bill@revealitconsulting.com>
 * @version   1.0.0-alpha.0
 * @date      2016-03-05 10:44:05
 * @note Change Log
 * - v1.0.0-alpha.0 - Initial rewrite version        - 2016-03-05 wer
 * - v0.1.0         - Initial version                - unknown wer
 * @todo CategoryTester.php - Everything, needs a rewrite
 */
namespace Ritc\Guide\Tests;

use Ritc\Guide\Model\Category;
use Ritc\Framework\Library\Elog;
use Ritc\Framework\Library\Files;
use Ritc\Framework\Library\Html;
use Ritc\Framework\Library\Tester;

/**
 * Class CategoryTester.
 * @class   CategoryTester
 * @package Ritc\Guide\Tests
 */
class CategoryTester extends Tester
{
    protected $a_test_order;
    protected $a_test_values = array();
    protected $failed_test_names;
    protected $failed_tests = 0;
    protected $num_o_tests;
    protected $o_elog;
    protected $o_files;
    protected $o_cat;
    protected $o_html;
    protected $passed_test_names  = array();
    protected $passed_tests = 0;
    public function __construct()
    {
        $this->o_elog  = Elog::start();
        $this->o_html  = new Html;
        $this->o_files = new Files('test_results.tpl', 'templates', 'default', 'Ritc\Guide');
        $this->o_cat   = new Category;
    }
    public function readCategoryTester()
    {
        $results1 = $this->o_cat->readCategory($this->a_test_values['readCategory1'], $this->a_test_values['readCategory1Params']);
        $this->o_elog->write('' . var_export($results1 , TRUE), LOG_OFF, __METHOD__ . '.' . __LINE__);
        if($this->compareArrays($this->a_test_values['readCategory1results'], $results1) === false) {
            $this->setSubfailure('readCategory', 'test1');
            return false;
        }
        $results2 = $this->o_cat->readCategory($this->a_test_values['readCategory2']);
        $this->o_elog->write('' . var_export($results2 , TRUE), LOG_OFF, __METHOD__ . '.' . __LINE__);
        if($this->compareArrays($this->a_test_values['readCategory2results'], $results2) === false) {
            $this->setSubfailure('readCategory', 'test2');
            return false;
        }
        $results3 = $this->o_cat->readCategory($this->a_test_values['readCategory3']);
        $this->o_elog->write('' . var_export($results3 , TRUE), LOG_OFF, __METHOD__ . '.' . __LINE__);
        if($this->compareArrays($this->a_test_values['readCategory3results'], $results3) === false) {
            $this->setSubfailure('readCategory', 'test3');
            return false;
        }
        return true;
    }
    public function readCatBySecTester()
    {
        $results1 = $this->o_cat->readCatBySec($this->a_test_values['readCatBySec1']);
        $this->o_elog->write('' . var_export($results1[0] , TRUE), LOG_OFF, __METHOD__ . '.' . __LINE__);
        if($this->compareArrays($this->a_test_values['readCatBySec1results'], $results1[0]) === false) {
            $this->setSubfailure('readCatBySec', 'test1');
            return false;
        }
        $results2 = $this->o_cat->readCatBySec($this->a_test_values['readCatBySec2']);
        $this->o_elog->write('results2' . var_export($results2 , TRUE), LOG_OFF, __METHOD__ . '.' . __LINE__);
        if($this->compareArrays($this->a_test_values['readCatBySec1results'], $results1[0]) === false) {
            $this->setSubfailure('readCatBySec', 'test2');
            return false;
        }
        return true;
    }
}
