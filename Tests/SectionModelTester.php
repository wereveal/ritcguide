<?php
/**
 * @brief     Tests the Section Model Class.
 * @ingroup   guide_tests
 * @file      SectionModelTester.php
 * @namespace Ritc\Guide\Tests
 * @author    William E Reveal <bill@revealitconsulting.com>
 * @version   1.0.0-alpha.0
 * @date      2016-03-05 10:44:05
 * @note Change Log
 * - v1.0.0-alpha.0 - Initial rewrite version        - 2016-03-05 wer
 * - v0.1.0         - Initial version                - unknown wer
 * @todo SectionModelTester.php - Everything, needs a rewrite
 */
namespace Ritc\Guide\Tests;

use Ritc\Guide\Models\SectionModel;
use Ritc\Library\Basic\Tester;
use Ritc\Library\Services\Di;

/**
 * Class SectionModelTester.
 * @class   SectionModelTester
 * @package Ritc\Guide\Tests
 */
class SectionModelTester extends Tester
{
    /** @var \Ritc\Library\Services\DbModel */
    protected $o_db;
    /** @var \Ritc\Library\Services\Di  */
    protected $o_di;
    /** @var \Ritc\Guide\Models\SectionModel  */
    protected $o_model;

    /**
     * SectionModelTester constructor.
     * @param \Ritc\Library\Services\Di $o_di
     */
    public function __construct(Di $o_di)
    {
        $this->o_db    = $o_di->get('db');
        $this->o_di    = $o_di;
        $this->o_model = new SectionModel($this->o_db);
        $a_setup = [
            'order_file'  => 'SectionModel_test_order.php',
            'values_file' => 'SectionModel_test_values.php',
            'extra_dir'   => 'config/tests',
            'theme'       => '',
            'namespace'   => 'Ritc\Guide'
        ];
        $this->setUpTests($a_setup);
        if (defined('DEVELOPER_MODE') && DEVELOPER_MODE) {
            $this->o_elog  = $o_di->get('elog');
            $this->o_model->setElog($this->o_elog);
        }
    }

    /**
     * Tests the create method.
     * @return bool
     */
    public function createTester()
    {
        $success = true;
        $x = 1;
        foreach ($this->a_test_values['create'] as $a_test) {
            $a_test_values = $a_test['test_value'];
            $expected_results = $a_test['expected_results'];
            $results = $this->o_model->create($a_test_values);
            if ($expected_results !== $results) {
                $this->setSubfailure('create', 'test' . $x);
                $success = false;
            }
            $x++;
        }
        return $success;
    }

    /**
     * Tests the read method.
     * @return bool
     */
    public function readTester()
    {
        return false;
    }

    /**
     * Tests the update method.
     * @return bool
     */
    public function updateTester()
    {
        return false;
    }

    /**
     * Tests the delete method.
     * @return bool
     */
    public function deleteTester()
    {
        return false;
    }

    /**
     * Tests the readDefaultSection method
     * @return bool
     */
    public function readDefaultSectionTester()
    {
        return false;
    }

    /**
     * Tests the readSectionById method
     * @return bool
     */
    public function readSectionByIdTester()
    {
        return false;
    }

}
