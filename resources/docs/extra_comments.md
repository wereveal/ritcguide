# Extra Comments {#extraComments}

## Complex Item Search {#complexItems}
a_search_for is an array to help define the search parameters in the complex sql.

### Basic Information

This is a multidimensional array and consists of three top level
arrays, category, est and data variables. This is an AND/OR search and can be quite complex. e.g.
WHERE (categories.name = American 
    OR categories.name = Coffee)
AND (
    (est.name = Denny 
      OR est.name = Applebee's 
      OR est.name = Blue
    )
    AND
    (est.featured = 1)
AND (city = Davenport OR city = Bettendorf)

However, there can be some searches that could be built that have mutually exclusive parameters. 
For example, est.name = Denny AND est.featured = 1 could return none. 

As such I would recommend that a lot of combinations be avoided. Of course, users will do
those kind of silly searches, like find all mexican restaurants in Davenport 
which have a name like Fred and serve reubens.

### Allowed Key/Value pairs

[
    'categories' => [
        'id'          => [''],
        'name'        => [''], 
        'description' => ['']
    ],
    'est' => [
        'name'        => [''],
        'active'      => '',
        'featured'    => '',
        'last_access' => '',
        'counter'     => '',
        'item_id'     => ['']
    ],
    'data' => [
        'name' => [
            '44 possibilities at this time, based on field name in the field table.
            Does a LIKE '%value%' comparison for field name so it needs to be similar',
        ],
        'description' => ['same as name']
    ]
]

### Caveat:

The search_parameters array that is also used in the function could change the AND/OR search to be all OR. As
such, the search would be
WHERE (categories.name = American 
    OR categories.name = Coffee)
OR (
    (est.name = Denny 
      OR est.name = Applebee's 
      OR est.name = Blue
    )
    OR
    (est.featured = 1)
OR (city = Davenport OR city = Bettendorf)

Of course, that specific search would return a lot of establishments, including all
establishments in Davenport and Bettendorf plus all American and Coffee est, 
plus all Denny's, Applebee's and every establishment that includes Blue in the name, 
including those that have (Blue Grass) in its name.

### Example array 

[
   'categories' => [
       'name' => [
           'American',
           'Coffee/Cafe'
       ]
   ],
   'est' => [
       'name' => [
           'Denny',
           "Applebee's",
           "Blue"
       ],
       'featured' => [
            1
       ]
    ],
   'data' => [
       'city' => [
           'Davenport',
           'Bettendorf'
       ]
   ]
]