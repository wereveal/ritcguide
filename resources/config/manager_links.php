<?php
return array(
    [
        'text'        => 'Home',
        'url'         => '/',
        'description' => 'Home Page',
        'name'        => 'Home',
        'class'       => '',
        'extras'      => '',
        'submenu'     => ''
    ],
    [
        'text'        => 'Manager Home',
        'url'         => '/manager/',
        'description' => 'Manager Home Page',
        'name'        => 'Manager Home',
        'class'       => '',
        'extras'      => '',
        'submenu'     => ''

    ],
    [
        'text'        => 'Back End Manager',
        'url'         => '/manager/backend/',
        'description' => 'Manager Back End Page',
        'name'        => 'Manager Back End',
        'class'       => '',
        'extras'      => '',
        'submenu'     => [
            [
                'text'        => 'Constants Manger',
                'url'         => '/manager/constants/',
                'description' => 'Constant values changed and new constants added.',
                'name'        => 'Constants',
                'class'       => '',
                'extras'      => '',
                'submenu'     => ''
            ],
            [
                'text'        => 'Routes Manager',
                'url'         => '/manager/routes/',
                'description' => 'Create and manage routes for the app.',
                'name'        => 'Routes',
                'class'       => '',
                'extras'      => '',
                'submenu'     => ''
            ],
            [
                'text'        => 'People Manager',
                'url'         => '/manager/people/',
                'description' => 'Create and manage people which get assigned to groups.',
                'name'        => 'People',
                'class'       => '',
                'extras'      => '',
                'submenu'     => ''
            ],
            [
                'text'        => 'Groups Manager',
                'url'         => '/manager/groups/',
                'description' => 'Create and manage groups to which people are assigned.',
                'name'        => 'Groups',
                'class'       => '',
                'extras'      => '',
                'submenu'     => ''
            ],
            [
                'text'        => 'Page Meta Manager',
                'url'         => '/manager/pages/',
                'description' => 'Create and manage page meta data.',
                'name'        => 'Pages',
                'class'       => '',
                'extras'      => '',
                'submenu'     => ''
            ],
            [
                'text'        => 'Tests',
                'url'         => '/manager/tests/',
                'description' => 'Tests',
                'name'        => 'Tests',
                'class'       => '',
                'extras'      => '',
                'submenu'     => ''
            ]
        ]
    ],
    [
        'text'        => 'Logout',
        'url'         => '/manager/logout/',
        'description' => 'Logout.',
        'name'        => 'Logout',
        'class'       => '',
        'extras'      => '',
        'submenu'     => ''
    ]
);
