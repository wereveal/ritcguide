<?php
$library_twig = APPS_PATH . '/Ritc/Library/resources/templates';
$guide_twig   = APPS_PATH . '/Ritc/Guide/resources/templates';
return array(
    'default_path'      => $guide_twig . '/pages',
    'additional_paths'  => array(
        $library_twig . '/default'  => 'default',
        $library_twig . '/elements' => 'elements',
        $library_twig . '/forms'    => 'forms',
        $library_twig . '/pages'    => 'pages',
        $library_twig . '/snippets' => 'snippets',
        $library_twig . '/tests'    => 'tests',
        $guide_twig   . '/default'  => 'guide_default',
        $guide_twig   . '/elements' => 'guide_elements',
        $guide_twig   . '/forms'    => 'guide_forms',
        $guide_twig   . '/pages'    => 'guide_pages',
        $guide_twig   . '/snippets' => 'guide_snippets',
        $guide_twig   . '/tests'    => 'guide_tests'
    ),
    'environment_options' => array(
        'cache' => SRC_PATH . '/twig_cache',
    )
);
