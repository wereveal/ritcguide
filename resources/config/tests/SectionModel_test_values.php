<?php
$a_first = [
    'sec_id'          => 1,
    'sec_name'        => 'test1',
    'sec_title'       => 'test1_title',
    'sec_description' => 'test1_should work',
    'sec_image'       => 'test1.png',
    'sec_order'       => 1,
    'sec_active'      => 1,
    'sec_default'     => 1
];
$a_second = [
    'sec_id'          => 2,
    'sec_name'        => 'test10',
    'sec_title'       => 'test10 title',
    'sec_description' => 'test10 should work',
    'sec_image'       => 'test10.png',
    'sec_order'       => 2,
    'sec_active'      => 1,
    'sec_default'     => 0
];

return [
    'read' => [
        [
            'test_value'       => ['sec_name' => 'test1'],
            'expected_results' => $a_first
        ],
        [
            'test_value'       => ['sec_id' => 2],
            'expected_results' => $a_second
        ],
        [
            'test_value'       => ['sec_id' => 3],
            'expected_results' => false
        ],
        [
            'test_value'       => '',
            'expected_results' => false
        ],
        [
            'test_value'       => ['fred' => 'flintstone'],
            'expected_results' => false
        ]
    ],
    'create' => [
        [
            'test_value'       => $a_first,
            'expected_results' => 1
        ],
        [
            'test_value'       => $a_second,
            'expected_results' => 2
        ],
        [
            'test_value'       => [
                'sec_name'        => 'test1',
                'sec_title'       => 'test2 title',
                'sec_description' => 'test2 should not work',
                'sec_image'       => 'test2.png',
                'sec_order'       => 1,
                'sec_active'      => 1,
                'sec_default'     => 1
            ],
            'expected_results' => false
        ],
        [
            'test_value'       => [
                'sec_id'          => 1,
                'sec_description' => 'test3 should not work'
            ],
            'expected_results' => false
        ],
        [
            'test_value'       => '',
            'expected_results' => false
        ]
    ],
    'update' => [
        [
            'test_value'       => [
                'sec_id'          => 1,
                'sec_name'        => 'test1',
                'sec_title'       => 'test1 the title',
                'sec_description' => 'test1 this should work'
            ],
            'expected_results' => true
        ],
        [
            'test_value'       => [
                'sec_id'          => 4,
                'sec_name'        => 'test1',
                'sec_title'       => 'test1 the title',
                'sec_description' => 'test1 this should work'
            ],
            'expected_results' => false
        ],
        [
            'test_value'       => [
                'sec_id'          => 2,
                'sec_name'        => 'test1',
            ],
            'expected_results' => false
        ],
        [
            'test_value'       => '',
            'expected_results' => ''
        ]
    ],
    'delete' => [
        [
            'test_value'       => ['sec_id' => 2],
            'expected_results' => true
        ],
        [
            'test_value'       => ['sec_id' => 1],
            'expected_results' => true
        ],
        [
            'test_value'       => ['sec_id' => 3],
            'expected_results' => false
        ],
        [
            'test_value'       => '',
            'expected_results' => false
        ]
    ],
    'readDefaultSection' => [
        [
            'test_value'       => '',
            'expected_results' => $a_first
        ],
        [
            'test_value'       => 'test',
            'expected_results' => false
        ],
    ],
    'readSectionById' => [
        [
            'test_value'       => 1,
            'expected_results' => $a_first
        ],
        [
            'test_value'       => 2,
            'expected_results' => $a_second
        ],
        [
            'test_value'       => 3,
            'expected_results' => false
        ],
        [
            'test_value'       => 'fred',
            'expected_results' => false
        ],
        [
            'test_value'       => '',
            'expected_results' => false
        ]
    ]
];