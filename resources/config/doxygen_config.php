<?php
/**
 * This file is used to help generate Doxygen Documentation.
 * It is similar to PhpDocumentor which is basically a subset of Doxygen.
 * @defgroup ritc_guide RitcGuide - a generic set of files used to create a guide or listing.
 * @ingroup ritc
 * @{
 *     @namespace Ritc\Guide
 *     @version 1.0
 *     @defgroup guide_abstracts Abstract class files.
 *     @ingroup ritc_guide
 *     @defgroup guide_controllers Controller files.
 *     @ingroup ritc_guide
 *     @defgroup guide_entities Generic accessors for data retreived from the database.
 *     @ingroup ritc_guide
 *     @defgroup guide_interfaces Files that define what a class should have.
 *     @ingroup ritc_guide
 *     @defgroup guide_models Classes that do database calls.
 *     @ingroup ritc_guide
 *     @defgroup guide_tests Classes that test other classes.
 *     @ingroup ritc_guide
 *     @defgroup guide_traits Functions that are common to multiple classes.
 *     @ingroup ritc_guide
 *     @defgroup guide_views Classes that provide the end user experience.
 *     @ingroup ritc_guide
 * @}
 */
