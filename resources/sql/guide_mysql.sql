CREATE TABLE `qcdg_section` (
  `sec_id` int(11) NOT NULL AUTO_INCREMENT,
  `sec_name` varchar(100) NOT NULL,
  `sec_title` text NOT NULL,
  `sec_description` text NOT NULL,
  `sec_image` text NOT NULL,
  `sec_order` int(11) NOT NULL,
  `sec_active` tinyint(1) NOT NULL,
  `sec_default` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`sec_id`),
  UNIQUE KEY `sec_name` (`sec_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `qcdg_field_type` (
  `ft_id` int(11) NOT NULL AUTO_INCREMENT,
  `ft_name` text NOT NULL,
  `ft_type` text NOT NULL,
  `ft_order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ft_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `qcdg_field` (
  `field_id` int(11) NOT NULL AUTO_INCREMENT,
  `field_type_id` int(11) NOT NULL DEFAULT '1',
  `field_name` varchar(48) NOT NULL,
  `field_short_description` text NOT NULL,
  `field_description` text NOT NULL,
  `field_enabled` tinyint(1) NOT NULL DEFAULT '1',
  `field_show_in` enum('both','vcard','details','hidden') NOT NULL DEFAULT 'both',
  PRIMARY KEY (`field_id`),
  UNIQUE KEY `field_name` (`field_name`),
  KEY `field_type_id` (`field_type_id`),
  CONSTRAINT `qcdg_field_ibfk_1` FOREIGN KEY (`field_type_id`) REFERENCES `qcdg_field_type` (`ft_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `qcdg_field_option` (
  `fo_id` int(11) NOT NULL AUTO_INCREMENT,
  `fo_field_id` int(11) NOT NULL,
  `fo_field_option` text NOT NULL,
  `fo_order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`fo_id`),
  KEY `fo_field_id` (`fo_field_id`),
  CONSTRAINT `qcdg_field_option_ibfk_1` FOREIGN KEY (`fo_field_id`) REFERENCES `qcdg_field` (`field_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `qcdg_field_sec_map` (
  `fs_id` int(11) NOT NULL AUTO_INCREMENT,
  `fs_field_id` int(11) NOT NULL,
  `fs_sec_id` int(11) NOT NULL,
  PRIMARY KEY (`fs_id`),
  UNIQUE KEY `fs_field_id` (`fs_field_id`,`fs_sec_id`),
  KEY `fs_sec_id` (`fs_sec_id`),
  CONSTRAINT `qcdg_field_sec_map_ibfk_1` FOREIGN KEY (`fs_field_id`) REFERENCES `qcdg_field` (`field_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `qcdg_field_sec_map_ibfk_2` FOREIGN KEY (`fs_sec_id`) REFERENCES `qcdg_section` (`sec_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `qcdg_category` (
  `cat_id` int(11) NOT NULL AUTO_INCREMENT,
  `cat_name` text NOT NULL,
  `cat_description` text NOT NULL,
  `cat_image` text NOT NULL,
  `cat_order` int(11) NOT NULL DEFAULT '0',
  `cat_active` tinyint(1) NOT NULL,
  `cat_default` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`cat_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `qcdg_cat_to_cat_map` (
  `ccm_id` int(11) NOT NULL AUTO_INCREMENT,
  `ccm_parent_id` int(11) NOT NULL,
  `ccm_child_id` int(11) NOT NULL,
  PRIMARY KEY (`ccm_id`),
  UNIQUE KEY `ccm_parent_child` (`ccm_parent_id`,`ccm_child_id`),
  KEY `ccm_child_id` (`ccm_child_id`),
  KEY `ccm_parent_id` (`ccm_parent_id`),
  CONSTRAINT `qcdg_cat_to_cat_map_ibfk_1` FOREIGN KEY (`ccm_parent_id`) REFERENCES `qcdg_category` (`cat_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `qcdg_sec_cat_map` (
  `sc_sec_id` int(11) NOT NULL,
  `sc_cat_id` int(11) NOT NULL,
  PRIMARY KEY (`sc_sec_id`,`sc_cat_id`),
  KEY `sc_cat_id` (`sc_cat_id`),
  CONSTRAINT `qcdg_sec_cat_map_ibfk_1` FOREIGN KEY (`sc_sec_id`) REFERENCES `qcdg_section` (`sec_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `qcdg_sec_cat_map_ibfk_2` FOREIGN KEY (`sc_cat_id`) REFERENCES `qcdg_category` (`cat_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `qcdg_item` (
  `item_id` int(11) NOT NULL AUTO_INCREMENT,
  `item_name` varchar(255) NOT NULL DEFAULT '',
  `item_created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `item_updated_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `item_active` tinyint(1) NOT NULL DEFAULT '1',
  `item_featured` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`item_id`),
  KEY `item_featured` (`item_featured`),
  KEY `item_name` (`item_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `qcdg_item_data` (
  `data_id` int(11) NOT NULL AUTO_INCREMENT,
  `data_field_id` int(11) NOT NULL,
  `data_item_id` int(11) NOT NULL,
  `data_text` text NOT NULL,
  `data_created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `data_updated_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`data_id`),
  KEY `oi_field_id` (`data_field_id`),
  KEY `ob_item_id` (`data_item_id`),
  CONSTRAINT `qcdg_item_data_ibfk_1` FOREIGN KEY (`data_item_id`) REFERENCES `qcdg_item` (`item_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `qcdg_cat_item_map` (
  `ci_id` int(11) NOT NULL AUTO_INCREMENT,
  `ci_cat_id` int(11) NOT NULL,
  `ci_item_id` int(11) NOT NULL,
  `ci_order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ci_id`),
  UNIQUE KEY `ci_combo` (`ci_cat_id`,`ci_item_id`),
  KEY `ci_category_id` (`ci_cat_id`),
  KEY `ci_item_id` (`ci_item_id`),
  CONSTRAINT `qcdg_category_item_ibfk_2` FOREIGN KEY (`ci_item_id`) REFERENCES `qcdg_item` (`item_id`),
  CONSTRAINT `qcdg_category_item_ibfk_1` FOREIGN KEY (`ci_cat_id`) REFERENCES `qcdg_category` (`cat_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
