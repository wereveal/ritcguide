<?php
/**
 * @brief     View Methods that are common to multiple views.
 * @ingroup   guide_traits
 * @file      GuideViewTraits.php
 * @namespace Ritc/Guide/Views
 * @author    William E Reveal  <bill@revealitconsulting.com>
 * @version   1.0.0-alpha.1
 * @date      2015-12-05 21:27:21
 * @note <b>Change Log</b>
 * - v1.0.0-alpha.1 - initial version                            - 12/05/2015 wer
 */
namespace Ritc\Guide\Traits;

use Ritc\Guide\Models\CategoryModel;
use Ritc\Guide\Models\ItemModel;
use Ritc\Guide\Models\SectionModel;
use Ritc\Library\Helper\Arrays;
use Ritc\Library\Helper\Strings;
use Ritc\Library\Traits\ViewTraits;

/**
 * Class GuideViewTraits
 * @class   GuideViewTraits
 * @package Ritc\Guide\Traits
 */
trait GuideViewTraits
{
    use ViewTraits;

    /**
     * @var
     */
    protected $a_menu;

    /**
     * Adds item data to the item records
     * @param array $a_items required
     * @param array $a_search_for_fields optional specific fields by name to search for e.g. ['about', 'phone']
     * @param array $a_search_parameters
     * @return array $a_items
     **/
    protected function addDataToItems(array $a_items = array(), array $a_search_for_fields = array(), array $a_search_parameters = array())
    {
        if ($a_items == array()) {
            return $a_items;
        }
        /** @var \Ritc\Library\Traits\LogitTraits $this->logIt */
        $this->logIt('' . var_export($a_items, TRUE), LOG_OFF, __METHOD__ . '.' . __LINE__);
        foreach ($a_items as $key => $a_item) {
            if ($a_search_for_fields != array()) { // searching for specific fields
                foreach ( $a_search_for_fields as $field_name) {
                    $a_search_for = array(
                        'data_item_id' => $a_item['item_id'],
                        'field_name' => $field_name
                    );
                    $a_item_data = $this->o_item->readItemData($a_item, $a_search_for, $a_search_parameters);
                    if (is_array($a_item_data) && count($a_item_data) > 0) {
                        $a_items[$key][$field_name] = strip_tags(stripslashes($a_item_data[0]['data_text']), "<br><a>");
                        if ($field_name == 'phone') {
                            $a_items[$key][$field_name] = Strings::formatPhoneNumber(
                                $a_items[$key][$field_name],
                                DISPLAY_PHONE_FORMAT
                            );
                        }
                    } else {
                        $a_items[$key][$field_name] = '';
                    }
                }
            } else { // get all fields for item
                $a_search_for = array('data_item_id' => $a_item['item_id']);
                $a_item_data = $this->o_item->readItemData($a_item, array(), $a_search_parameters);
                if (is_array($a_item_data) && count($a_item_data) > 0) {
                    $a_item_data = Arrays::removeSlashes($a_item_data);
                    $a_item = array_merge($a_item, $a_item_data);
                    $a_items[$key] = $a_item;
                }
            }
        }
        return $a_items;
    }
    /**
     * Creates the values to be used in the twig tpl.
     * @param string $current_letter optional, defaults to ''
     * @return array $a_values
     **/
    protected function alphaList($current_letter = '')
    {
        $o_item = new ItemModel();
        $main_str = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        $a_li_values  = array();
        $first_letter = $main_str[0];
        $last_letter  = $main_str[count($main_str) - 1];
        for ($i = 0; $i < strlen($main_str); $i++) {
            $the_letter = $main_str[$i];
            $the_class  = 'otherLetter';
            $is_link    = true;
            $results = $o_item->readItemByNameFirstLetter($the_letter);
            /** @var \Ritc\Library\Traits\LogitTraits $this->logIt */
            $this->logIt("item list\n" . var_export($results, true), LOG_OFF, __METHOD__ . '.' . __LINE__);
            if ($results != false && count($results) > 0) {
                if ($the_letter == $first_letter) {
                    $the_class = 'firstLetter';
                }
                elseif ($the_letter == $last_letter) {
                    $the_class = 'lastLetter';
                }
                if ($the_letter == $current_letter) {
                    $the_class .= ' currentLetter';
                    $is_link = false;
                }
            }
            else {
                $the_class = 'noLink';
                $is_link   = false;
            }
            $a_li_row = array(
                'the_letter' => $the_letter,
                'the_class'  => $the_class,
                'is_link'    => $is_link,
            );
            $a_li_values[$i] = $a_li_row;
        }
        return $a_li_values;
    }
    /**
     * Creates the values to be used in the category select.
     * @param int $section_id defaults to 1
     * @param string $selected_category
     * @return array $a_categories
     **/
    protected function categoryList($section_id = 1, $selected_category = '')
    {
        $o_category = new CategoryModel();
        $a_return_this = array(
            'name'        => '',
            'class'       => '',
            'other_stuph' => '',
            'options'     => '',
            'label_for'   => '',
            'label_text'  => '',
            'label_class' => ''
        );
        $a_categories = $o_category->readCatBySec($section_id);
        /** @var \Ritc\Library\Traits\LogitTraits $this->logIt */
        $this->logIt("categories\n" . var_export($a_categories, true), LOG_OFF, __METHOD__ . '.' . __LINE__);
        if (count($a_categories) > 1) {
            foreach ($a_categories as $a_category) {
                if ($selected_category == '') {
                    $selected_category = $a_category['cat_id'];
                }
                $a_return_this['options'][] = array(
                    'value'       => $a_category['cat_id'],
                    'label'       => $a_category['cat_name'],
                    'other_stuph' => $selected_category == $a_category['sec_id'] ? ' selected' : ''
                );
            }
            $a_return_this['name']        = 'catSelect';
            $a_return_this['class']       = 'catSelect';
            $a_return_this['other_stuph'] = ' id="catSelect" onchange="searchByCategory(this)"';
            $a_return_this['label_for']   = 'catSelect';
            $a_return_this['label_text']  = 'Search By Category';
            $a_return_this['label_class'] = 'selectLabel';
        }
        return $a_return_this;
    }
    /**
     * Returns the data needed for the quick search form.
     * @param array $a_search_for
     * @return array
    **/
    protected function formQuickSearch(array $a_search_for = array())
    {
        $search_str = '';
        if ($a_search_for != array()) {
            foreach ($a_search_for as $value) {
                if (strpos($value, ' ') !== false) {
                    $value = '"' . $value . '"';
                }
                $search_str .= $value . ' ';
            }
        }
        return array(
            'buttonColor'   => 'white',
            'buttonText'    => 'Locate',
            'searchForText' => $search_str,
            'helpicon'      => ''
        );
    }
    /**
     * Returns the default section to use in the display.
     * @return int
     */
    protected function getDefaultSection()
    {
        return 1;
    }
    /**
     * Returns the default number of items to display in a list.
     * @return int
     */
    protected function getNumToDisplay()
    {
        return 10;
    }
    /**
     * Sets up the common template values
     * @return array
     */
    protected function initializeTplValues()
    {
        $a_quick_form    = $this->formQuickSearch();
        $a_alpha_list    = $this->alphaList();
        $a_section_list  = $this->sectionList($this->default_section);
        $a_category_list = $this->categoryList($this->default_section);
        return array(
            'title'         => 'Guide',
            'description'   => 'This is a description',
            'site_url'      => SITE_URL,
            'rights_holder' => RIGHTS_HOLDER,
            'quick_form'    => $a_quick_form,
            'alpha_list'    => $a_alpha_list,
            'section_list'  => $a_section_list,
            'category_list' => $a_category_list,
            'item_cards'    => ''
        );
    }
    /**
     * Creates the values needed for the section list
     * @param mixed $selected_section can be an id or a name
     * @param array $a_search_parameters
     *     (see o_sec->readSection comments for more info)
     * @return array $a_return_this
     **/
    protected function sectionList($selected_section = '', array $a_search_parameters = array())
    {
        $o_section  = new SectionModel();
        $a_return_this = array(
            'name'        => '',
            'class'       => '',
            'other_stuph' => '',
            'options'     => '',
            'label_for'   => '',
            'label_text'  => '',
            'label_class' => ''
        );
        $a_sections = $o_section->read('', $a_search_parameters);
        /** @var \Ritc\Library\Traits\LogitTraits $this->logIt */
        $this->logIt("selects returned\n" . var_export($a_sections, true), LOG_OFF, __METHOD__ . '.' . __LINE__);
        if (count($a_sections) > 1) {
            foreach ($a_sections as $a_section) {
                if ($selected_section == '') {
                    $selected_section = $a_section['sec_id'];
                }
                $a_return_this['options'][] = array(
                    'value'     => $a_section['sec_id'],
                    'label'     => $a_section['sec_name'],
                    'other_stuph' => $selected_section == $a_section['sec_id'] ? ' selected' : ''
                );
            }
            $a_return_this['name']        = 'sectionSelect';
            $a_return_this['class']       = 'sectionSelect';
            $a_return_this['other_stuph'] = ' id="sectionSelect" onchange="searchBySection(this)"';
            $a_return_this['label_for']   = 'sectionSelect';
            $a_return_this['label_text']  = 'Search By Section';
            $a_return_this['label_class'] = 'selectLabel';
        }
        $this->logIt("select list array\n" . var_export($a_return_this, true), LOG_OFF, __METHOD__ . '.' . __LINE__);
        return $a_return_this;
    }
    /**
     * Sets an array used for the for the menus.
     * @return null
     */
    protected function setMenu()
    {
        $a_menu = array();
        if (file_exists(SRC_CONFIG_PATH . '/menu_config.php')) {
            $a_menu = include SRC_CONFIG_PATH . '/menu_config.php';
        }
        $this->a_menu = $a_menu;
    }
}
