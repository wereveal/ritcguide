# README #

This is a basic skeleton/framework upon which one can build a guide.

## The Reveal IT Consulting Guide framework ##

* This is framework from which a guide can be created.
* Version 1.0.0

## How do I get set up? ##

* Clone the RITC Framework
* Clone the RITC Library
* Clone this repo
* Configuration - just install the database
* Dependencies - RITC Library
* Database configuration see resources/sql/guide_mysql.sql or guide_pgsql.sql

## Contribution guidelines ##

* See the RITC Library for recommended contribution guidelines.

## Who do I talk to? ##

* William E Reveal <bill@revealitconsulting.com>
* Other community or team contact
    * None at this time