<?php
/**
 * @brief     Does the CRUD for the cat_to_cat_map table..
 * @ingroup   guide_models
 * @file      CatCatMapModel.php
 * @namespace Ritc\Guide\Models
 * @author    William E Reveal <bill@revealitconsulting.com>
 * @version   1.0.0-alpha.0+2
 * @date      2016-04-01 09:26:50
 * @note Change Log
 * - v1.0.0-alpha.0 - Initial version        - 2016-03-23 wer
 */
namespace Ritc\Guide\Models;

use Ritc\Library\Interfaces\ModelInterface;
use Ritc\Library\Services\DbModel;
use Ritc\Library\Traits\DbUtilityTraits;
use Ritc\Library\Traits\LogitTraits;

/**
 * Class CatCatMapModel.
 * @class   CatCatMapModel
 * @package Ritc\Guide\Models
 */
class CatCatMapModel implements ModelInterface
{
    use LogitTraits, DbUtilityTraits;

    /**
     * CatCatMapModel constructor.
     * @param \Ritc\Library\Services\DbModel $o_db
     */
    public function __construct(DbModel $o_db)
    {
        $this->setupProperties($o_db, 'cat_to_cat_map');
    }

    /**
     * General create method.
     * @param array $a_values
     * @return array|bool
     */
    public function create(array $a_values = [])
    {
        $meth = __METHOD__ . '.';
        if ($a_values == []) {
            $this->error_message = 'No values provided for insert. ';
            return false;
        }
        $a_params = [
            'a_required_keys' => [
                'ccm_parent_id',
                'ccm_child_id'
            ],
            'a_field_names'   => $this->a_db_fields,
            'a_psql'          => [
                'table_name'  => $this->db_table,
                'column_name' => 'ccm_id'
            ]
        ];
        $log_message = 'create parameters ' . var_export($a_params, TRUE);
        $this->logIt($log_message, LOG_OFF, $meth . __LINE__);

        return $this->genericCreate($a_values, $a_params);
    }

    /**
     * General read table method.
     * @param array $a_search_for
     * @param array $a_params \ref searchparams \ref readparams
     * @return array|bool
     */
    public function read(array $a_search_for = [], array $a_params = [])
    {
        $a_parameters = [
            'table_name'   => $this->db_table,
            'a_search_for' => $a_search_for,
            'a_fields'     => $this->a_db_fields,
            'order_by'     => 'ccm_parent_id ASC, ccm_child_id ASC'
        ];
        $a_parameters = array_merge($a_parameters, $a_params);
        return $this->genericRead($a_parameters);
    }

    /**
     * General update record method.
     * @param array $a_values
     * @return bool
     */
    public function update(array $a_values = [])
    {
        if ($a_values == []) {
            $this->error_message = 'No values provided for update.';
            return false;
        }
        return $this->genericUpdate($a_values);
    }

    /**
     * General delete record method.
     * @param int $id
     * @return bool
     */
    public function delete($id = -1)
    {
        if ($id == -1) {
            $this->error_message = 'A bad id was provided.';
            return false;
        }
        return $this->genericDelete($id);
    }
}
