<?php
/**
 * @brief     The CRUD for the Field to Section Map table (field_sec_map).
 * @ingroup   guide_models
 * @file      FieldSecMapModel.php
 * @namespace Ritc\Guide\Models
 * @author    William E Reveal <bill@revealitconsulting.com>
 * @version   1.0.0-alpha.1+2
 * @date      2016-04-01 09:28:46
 * @note Change Log
 * - v1.0.0-alpha.2 - bug fix, read method needed to match interface    - 2016-03-24 wer
 * - v1.0.0-alpha.1 - Refactoring of DbModel reflected here             - 2016-03-18 wer
 * - v1.0.0-alpha.0 - Initial version                                   - 2016-03-09 wer
 */
namespace Ritc\Guide\Models;

use Ritc\Library\Interfaces\ModelInterface;
use Ritc\Library\Services\DbModel;
use Ritc\Library\Traits\DbUtilityTraits;
use Ritc\Library\Traits\LogitTraits;

/**
* Class FieldSecMapModel.
* @class   FieldSecMapModel
* @package Ritc\Guide\Models
*/
class FieldSecMapModel implements ModelInterface
{
    use LogitTraits, DbUtilityTraits;

    /**
     * FieldModel constructor.
     * @param \Ritc\Library\Services\DbModel $o_db
     */
    public function __construct(DbModel $o_db)
    {
        $this->setupProperties($o_db, 'field_sec_map');
    }

    /**
     * Generic create a record using the values provided.
     * @param array $a_values
     * @return bool
     */
    public function create(array $a_values = [])
    {
        $a_parameters['a_required_keys'] = [
            ':fs_field_id',
            ':fs_sec_id'
        ];
        $a_parameters['a_psql'] = [
            'table_name' => $this->db_table,
            'column_name' => 'fs_id'
        ];
        return $this->genericCreate($a_values, $a_parameters);
    }

    /**
     * Returns an array of records based on the search params provided.
     * @param array $a_search_values
     * @param array $a_parameters    \ref searchparams \ref readparams
     * @return array|bool
     */
    public function read(array $a_search_values = [], array $a_parameters = [])
    {
        $a_params = [
            'table_name'   => $this->db_table,
            'a_fields'     => $this->a_db_fields,
            'a_search_for' => $a_search_values,
            'order_by'     => ['fs_sec_id ASC, fs_order ASC']
        ];
        $a_parameters = array_merge($a_params, $a_parameters);
        return $this->genericRead($a_parameters);
    }

    /**
     * Generic update for a record using the values provided.
     * @param array $a_values
     * @return bool
     */
    public function update(array $a_values = [])
    {
        return $this->genericUpdate($a_values);
    }

    /**
     * Generic deletes a record based on the id provided.
     * @param int $id
     * @return bool
     */
    public function delete($id = -1)
    {
        return $this->genericDelete($id);
    }

}
