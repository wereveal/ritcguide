<?php
/**
 * @brief     Does complex (e.g. join) queries for item, item_data and field tables..
 * @ingroup   guide_models
 * @file      ItemComplexModel.php
 * @namespace Ritc\Guide\Models
 * @author    William E Reveal <bill@revealitconsulting.com>
 * @version   1.0.0-alpha.0
 * @date      2016-04-20 08:08:09
 * @note Change Log
 * - v1.0.0-alpha.0 - Initial version        - 2016-04-20 wer
 */
namespace Ritc\Guide\Models;

use Ritc\Library\Services\DbModel;
use Ritc\Library\Traits\DbUtilityTraits;
use Ritc\Library\Traits\LogitTraits;

/**
 * Class ItemComplexModel.
 * @class   ItemComplexModel
 * @package Ritc\Guide\Models
 */
class ItemComplexModel
{
    use LogitTraits, DbUtilityTraits;

    public function __construct(DbModel $o_db)
    {
        $this->setupProperties($o_db);
    }

    /**
     * Returns the record for the item data specified
     * @param array $a_item_ids          item ids to return data
     * @param array $a_search_for        optional will return everything by default
     * @param array $a_search_parameters optional see Database class for parameters and defaults
     * @return mixed array or false
     */
    public function readItemData(array $a_item_ids = [], array $a_search_for = [], array $a_search_parameters = [])
    {
        if ($a_item_ids == array()) {
            return array();
        }
        $a_item_ids = $this->o_db->prepareKeys($a_item_ids);
        if ($a_search_parameters == array()) {
            $a_search_parameters = array(
                "order_by"     => "f.field_short_description"
            );
        }
        $a_search_parameters['where_exists'] = true; // force it to be true, even if set to false in the argument $a_search_parameters
        $sql_where = $this->buildSqlWhere($a_search_for, $a_search_parameters);
        $sql = "
            SELECT d.data_id, d.data_item_id as item_id, d.data_text, 
                   f.field_name, f.field_short_description
            FROM {$this->db_prefix}item_data as d, {$this->db_prefix}field as f
            WHERE d.data_field_id = f.field_id
            AND d.data_item_id = :data_item_id
            {$sql_where}
        ";
        $results = $this->o_db->search($sql, $a_item_ids);
        if ($results === false) {
            $this->logIt('Error Msg: ' . $this->o_db->getSqlErrorMessage(), LOG_OFF, __METHOD__ . '.' . __LINE__);
        }
        $this->logIt('Item data: ' . var_export($results, true), LOG_OFF, __METHOD__ . '.' . __LINE__);
        return $results;
    }

}