<?php
/**
 * @brief     Handles all the database CRUD for the Section Catgegory Map
 * @ingroup   guide_models
 * @file      SecCatModel.php
 * @namespace Ritc\Guide\Models
 * @author    William E Reveal <bill@revealitconsulting.com>
 * @version   1.0.0-alpha.2
 * @date      2016-04-20 08:02:07
 * @note <b>Change Log</b>
 * - v1.0.0-alpha.2 - Added deleteByCategory and wrote deleteBySec methods  - 2016-04-20 wer
 * - v1.0.0-alpha.1 - Refactored to use DbUtilityTraits                     - 2016-03-26 wer
 * - v1.0.0-alpha.0 - initial version                                       - 2016-02-22 wer
 */
namespace Ritc\Guide\Models;

use Ritc\Library\Helper\Arrays;
use Ritc\Library\Interfaces\ModelInterface;
use Ritc\Library\Services\DbModel;
use Ritc\Library\Traits\DbUtilityTraits;
use Ritc\Library\Traits\LogitTraits;

/**
 * Class SecCatModel
 * @class     SecCatModel
 * @package Ritc\Guide\Models
 */
class SecCatModel implements ModelInterface
{
    use LogitTraits, DbUtilityTraits;

    /**
     * SecCatModel constructor.
     * @param \Ritc\Library\Services\DbModel $o_db
     */
    public function __construct(DbModel $o_db)
    {
        $this->setupProperties($o_db, 'sec_cat_map');
    }

    ### CRUD methods required by Interface ###
    /**
     * Creates a new record in the {$db_prefix}section table.
     * @param array $a_query_values
     * @return int|bool
    **/
    public function create(array $a_query_values = [])
    {
        $a_parameters = [
            'a_required_keys' => [
                'sc_sec_id',
                'sc_cat_id'
            ],
            'a_field_names'   => $this->a_db_fields,
            'a_psql'          => [
                'table_name'  => $this->db_table,
                'column_name' => 'sc_id'
            ]
        ];
        return $this->genericCreate($a_query_values, $a_parameters);
    }

    /**
     * Returns one or more records from the {$db_prefix}section table.
     * @param array $a_search_for        optional, assoc array field_name=>field_value
     * @param array $a_search_parameters optional allows one to specify various settings
     * @note Not all parameters need to be in the array, if doesn't exist, the default setting will be used.
     * @ref searchparams See for what is allowed.
     * @return array $a_records
    **/
    public function read(array $a_search_for = [], array $a_search_parameters = [])
    {
        $a_parameters = [
            'table_name'   => $this->db_table,
            'a_search_for' => $a_search_for,
            'a_fields'     => $this->a_db_fields,
            'order_by'     => 'sc_sec_id ASC, sc_cat_id ASC'
        ];
        $a_parameters = array_merge($a_parameters, $a_search_parameters);
        return $this->genericRead($a_parameters);
    }

    /**
     * Updates a record in the {$db_prefix}sec_cat_map table.
     * NOT! This is a mapping that basically is either added new, searched for
     * or deleted. Never updated. Method exists because it is required by
     * interface. This forces the programmer to think about what they are doing.
     * @param array $a_query_values
     * @return bool
    **/
    public function update(array $a_query_values = array())
    {
        return false;
    }

    /**
     * Deletes the record specified by primary key.
     * @param int $sc_id required
     * @return bool success or failure
     */
    public function delete($sc_id = -1)
    {
        if ($sc_id == -1) { return false; }
        return $this->genericDelete($sc_id);
    }

    ### Extra Read Methods ###
    /**
     * Gets the records from {$db_prefix}sec_cat_map which match the category id
     * @param int $cat_id
     * @return bool|array $a_records
     **/
    public function readMapByCatId($cat_id = -1)
    {
        if ($cat_id == -1 || !is_numeric($cat_id)) {
            return false;
        }
        $a_search_for    = [':sc_cat_id' => $cat_id];
        $a_search_params = ['order_by' => 'sc_sec_id ASC, sc_cat_id ASC'];
        return $this->read($a_search_for, $a_search_params);
    }

    /**
     * Returns a record from the {$db_prefix}section table
     * @param int $sec_id Required
     * @return array|bool $a_record
     */
    public function readMapBySecId($sec_id = -1)
    {
        if ($sec_id == -1 || !is_numeric($sec_id)) {
            return false;
        }
        $a_search_for    = [':sc_sec_id' => $sec_id];
        $a_search_params = ['order_by' => 'sc_sec_id ASC, sc_cat_id ASC'];
        return $this->read($a_search_for, $a_search_params);
    }

    ### Extra Delete Methods ###
    /**
     * Deletes all the records which have the category id supplied.
     * @param int $cat_id Required.
     * @return bool
     */
    public function deleteByCategory($cat_id)
    {
        if ($cat_id == -1 || !is_numeric($cat_id)) {
            return false;
        }
        $sql =<<<SQL
DELETE FROM {$this->db_table}
WHERE sc_cat_id = :sc_cat_id
SQL;
        $a_values = [':sc_cat_id' => $cat_id];
        return $this->o_db->delete($sql, $a_values);
    }

    /**
     * Deletes multiple records, all that correspond to the section id.
     * @param int $section_id
     * @return bool
     */
    public function deleteBySection($section_id)
    {
        if ($section_id == -1 || !is_numeric($section_id)) {
            return false;
        }
        $sql =<<<SQL
DELETE FROM {$this->db_table}
WHERE sc_sec_id = :sc_sec_id
SQL;
        $a_values = [':sc_sec_id' => $section_id];
        return $this->o_db->delete($sql, $a_values);
    }

    ### Utilities ###
}
