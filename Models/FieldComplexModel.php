<?php
/**
 * @brief     Handles complex queries for the Fields.
 * @details   For example, joins, subselects, and transactions.
 * @ingroup   guide_models
 * @file      FieldComplexModel.php
 * @namespace Ritc\Guide\Models
 * @author    William E Reveal <bill@revealitconsulting.com>
 * @version   1.0.0-alpha.1
 * @date      2016-03-18 15:41:29
 * @note <b>Change Log</b>
 * - v1.0.0-alpha.1 - Refactoring of the Library DbModel class reflected here       - 2016-03-18 wer
 * - v1.0.0-alpha.0 - initial version                                               - 03/04/2016 wer
 *  @TODO everything
 */
namespace Ritc\Guide\Models;

use Ritc\Library\Services\DbModel;
use Ritc\Library\Traits\DbUtilityTraits;
use Ritc\Library\Traits\LogitTraits;

/**
 * Class FieldComplexModel
 * @package Ritc\Guide\Models
 */
class FieldComplexModel
{
    use LogitTraits, DbUtilityTraits;

    /** @var string  */
    protected $field_table;
    /** @var string  */
    protected $fs_table;

    /**
     * FieldComplexModel constructor.
     * @param \Ritc\Library\Services\DbModel $o_db
     */
    public function __construct(DbModel $o_db)
    {
        $this->setupProperties($o_db, 'field');
        $this->field_table = $this->db_table;
        $this->fs_table    = $this->db_prefix . 'field_section';
    }

    /**
     *  Returns the custom field info for a section.
     *  @param int $section_id
     *  @return array $a_fields
     */
    public function readFieldBySection($section_id = 1)
    {
        $meth = __METHOD__ . '.';
        $sql = "
            SELECT f.field_id, f.field_type_id, f.field_name
            FROM {$this->field_table} as f, {$this->fs_table} as fs
            WHERE fs.fc_field_id = f.field_id
            AND fs.fc_sec_id = {$section_id}
            AND f.field_enabled = 1
        ";
        $a_values = $this->o_db->search($sql);
        $this->o_elog->write(var_export($a_values, true), LOG_OFF, $meth . __LINE__);
        return $a_values;
    }

}
