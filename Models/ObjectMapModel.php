<?php
/**
 * @brief     Handles all the database needs (CRUD) for the ObjectMap table
 * @details   This is a temporary thing, designed primarily for the migrate from
 *            Sobi to this app.
 * @ingroup   guide_models
 * @file      ObjectMapModel.php
 * @namespace Ritc\Guide\Models
 * @author    William E Reveal <bill@revealitconsulting.com>
 * @version   1.0.0-alpha.2
 * @date      2016-04-20 07:36:19
 * @note <b>Change Log</b>
 * - v1.0.0.alpha.2 - Bug Fix                                                         - 2016-04-20 wer
 * - v1.0.0-alpha.1 - After Adding DbUtilityTraits                                    - 2016-03-23 wer
 * - v1.0.0-alpha.0 - initial version                                                 - 2016-03-05 wer
 */
namespace Ritc\Guide\Models;

use Ritc\Library\Interfaces\ModelInterface;
use Ritc\Library\Services\DbModel;
use Ritc\Library\Traits\DbUtilityTraits;
use Ritc\Library\Traits\LogitTraits;

/**
 * Class ObjectMapModel.
 * @class ObjectMapModel
 * @package Ritc\Guide\Models
 */
class ObjectMapModel implements ModelInterface
{
    use LogitTraits, DbUtilityTraits;

    /**
     * ObjectMapModel constructor.
     * @param \Ritc\Library\Services\DbModel $o_db
     */
    public function __construct(DbModel $o_db)
    {
        $this->setupProperties($o_db, 'object_map');
    }

    ### CRUD methods required by Interface ###
    /**
     * Creates a new record in the {$db_prefix}object_map table
     * @param array $a_query_values ['old_id', 'new_id', 'obj_type']
     * @return int|bool
    **/
    public function create(array $a_query_values = [])
    {
        $a_required_keys = [
            'old_id',
            'new_id',
            'obj_type'
        ];
        $a_psql = [
            'table_name'  => $this->db_table,
            'column_name' => 'obj_id'
        ];
        $a_parameters = [
            'a_required_keys' => $a_required_keys,
            'a_field_names'   => $this->a_db_fields,
            'a_psql'          => $a_psql
        ];
        return $this->genericCreate($a_query_values, $a_parameters);
    }

    /**
     * Returns one or more records from the {$db_prefix}section table
     * @param array $a_search_for        optional, assoc array field_name=>field_value
     * @param array $a_search_parameters optional allows one to specify various settings
     *
     * @ref searchparams for more info on a_search_parameters.
     *
     * @return array $a_records
    **/
    public function read(array $a_search_for = [], array $a_search_parameters = [])
    {
        $meth = __METHOD__ . '.';
        $a_parameters = $a_search_parameters == []
            ? ['order_by' => 'obj_type ASC, new_id ASC']
            : $a_search_parameters;
        $a_parameters['a_search_for'] = $a_search_for;
        $a_parameters['table_name'] = $this->db_table;
        $log_message = 'a_parameters ' . var_export($a_parameters, TRUE);
        $this->logIt($log_message, LOG_OFF, $meth . __LINE__);

        return $this->genericRead($a_parameters);
    }

    /**
     * Updates a record in the {$db_prefix}section table
     * @param array $a_query_values
     * @return bool success or failure
    **/
    public function update(array $a_query_values = [])
    {
        return $this->genericUpdate($a_query_values);
    }

    /**
     * Deletes the record specified by id
     * @param int $record_id required
     * @return bool success or failure
     **/
    public function delete($record_id = -1)
    {
        if ($record_id == -1 || !is_numeric($record_id)) {
            return false;
        }
        $o_sec_cat = new SecCatModel($this->o_db);
        $a_results = $o_sec_cat->readMapBySecId($record_id);
        if ($a_results !== false && count($a_results) > 0) {
            $this->error_message = "There are categories still assigned to this section.";
            return false; // a category still exists in the section
        }
        return $this->genericDelete($record_id);
    }

    ### Read Methods ###
    /**
     * Returns the default section.
     * It should be noted that this assumes only one default section.
     * @return bool
     */
    public function readDefaultSection()
    {
        $a_search_for = [':sec_default' => 1];
        return $this->read($a_search_for);
    }

    /**
     * Returns a record from the {$db_prefix}section table
     * @param int $record_id required
     * @return bool|array $a_record
    **/
    public function readSectionById($record_id = -1)
    {
        if ($record_id == -1 || !is_numeric($record_id)) {
            return false;
        }
        $a_search_for = [':sec_id' => $record_id];
        return $this->read($a_search_for);
    }

    ### Update Methods ###

    ### Delete Methods ###

    ### Utilities ###
}
