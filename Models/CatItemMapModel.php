<?php
/**
 * @brief     Handles the CRUD for the cat_item_map table.
 * @ingroup   guide_models
 * @file      CatItemMapModel.php
 * @namespace Ritc\Guide\Models
 * @author    William E Reveal <bill@revealitconsulting.com>
 * @version   1.0.0-alpha.1
 * @date      2016-04-20 08:22:31
 * @note Change Log
 * - v1.0.0-alpha.1 - Added 2 addtional methods to delete by item id and cat id - 2016-04-20 wer
 * - v1.0.0-alpha.0 - Initial version                                           - 2016-03-23 wer
 */
namespace Ritc\Guide\Models;

use Ritc\Library\Interfaces\ModelInterface;
use Ritc\Library\Services\DbModel;
use Ritc\Library\Traits\DbUtilityTraits;
use Ritc\Library\Traits\LogitTraits;

/**
 * Class CatItemMapModel which maps which categories an item belongs in.
 * This allows an item to belong to multiple categories.
 * @class   CatItemMapModel
 * @package Ritc\Guide\Models
 */
class CatItemMapModel implements ModelInterface
{
    use LogitTraits, DbUtilityTraits;

    /**
     * CatItemMapModel constructor.
     * @param \Ritc\Library\Services\DbModel $o_db
     */
    public function __construct(DbModel $o_db)
    {
        $this->setupProperties($o_db, 'cat_item_map');
    }

    /**
     * Creates the record.
     * @param array $a_values
     * @return array|bool
     */
    public function create(array $a_values = [])
    {
        if ($a_values == []) {
            $this->error_message = "Requires ci_cat_id and ci_item_id";
            return false;
        }
        $a_parameters = [
            'a_required_keys' => [
                'ci_cat_id',
                'ci_item_id'
            ],
            'a_field_names'   => $this->a_db_fields,
            'a_psql'          => [
                'table_name'  => $this->db_table,
                'column_name' => 'ci_id'
            ],
        ];
        return $this->genericCreate($a_values, $a_parameters);
    }

    /**
     * Reads records from the database.
     * @param array $a_search_values
     * @param array $a_search_params
     * return bool
     */
    public function read(array $a_search_values = [], array $a_search_params = [])
    {
        $a_search_params['a_search_for'] = $a_search_values;
        if (!isset($a_search_params['table_name'])) {
            $a_search_params['table_name'] = $this->db_table;
        }
        if (!isset($a_search_params['a_fields'])) {
            $a_search_params['a_fields'] = $this->a_db_fields;
        }
        if (!isset($a_search_params['order_by'])) {
            $a_search_params['order_by'] = 'ci_cat_id ASC, ci_item_id ASC';
        }
        return $this->genericRead($a_search_params);
    }

    /**
     * Updates the records.
     * @param array $a_values
     * @return bool
     */
    public function update(array $a_values)
    {
        return $this->genericUpdate($a_values);
    }

    /**
     * Deletes a record by its primary id.
     * @param int $id
     * @return bool
     */
    public function delete($id = -1)
    {
        return $this->genericDelete($id);
    }

    ### Delete By ###

    /**
     * Deletes record(s) by category id.
     * @param int $cat_id
     * @return bool
     */
    public function deleteByCategory($cat_id = -1)
    {
        if ($cat_id == -1 || !is_numeric($cat_id)) {
            return false;
        }
        $sql =<<<SQL
DELETE FROM {$this->db_table}
WHERE ci_cat_id = :ci_cat_id
SQL;
        $a_values = [':ci_cat_id' => $cat_id];
        return $this->o_db->delete($sql, $a_values);
    }

    /**
     * Delete record(s) by item id.
     * @param int $item_id
     * @return bool
     */
    public function deleteByItem($item_id = -1)
    {
        if ($item_id == -1 || !is_numeric($item_id)) {
            return false;
        }
        $sql =<<<SQL
DELETE FROM {$this->db_table}
WHERE ci_item_id = :ci_item_id
SQL;
        $a_values = [':ci_item_id' => $item_id];
        return $this->o_db->delete($sql, $a_values);
    }
}