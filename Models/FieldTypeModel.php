<?php
/**
 * @brief     Handles all the database needs (CRUD) for the Field Type table
 * @ingroup   guide_models
 * @file      FieldTypeModel.php
 * @namespace Ritc\Guide\Models
 * @author    William E Reveal <bill@revealitconsulting.com>
 * @version   1.0.0-alpha.1
 * @date      2016-04-20 07:33:12
 * @note <b>Change Log</b>
 * - v1.0.0-alpha.1 - Added DbUtilityTraits                                           - 2016-04-20 wer
 * - v1.0.0-alpha.0 - start of rewrite                                                - 12/20/2015 wer
 * - v0.1.0 - initial version
 * @TODO FieldTypeModel.php - implement DbUtilityTraits
 */
namespace Ritc\Guide\Models;

use Ritc\Library\Helper\Arrays;
use Ritc\Library\Interfaces\ModelInterface;
use Ritc\Library\Services\DbModel;
use Ritc\Library\Traits\DbUtilityTraits;
use Ritc\Library\Traits\LogitTraits;

/**
 * Class FieldTypeModel
 * @class   FieldTypeModel
 * @package Ritc\Guide\Models
 */
class FieldTypeModel implements ModelInterface
{
    use LogitTraits, DbUtilityTraits;

    /**
     * FieldTypeModel constructor.
     * @param \Ritc\Library\Services\DbModel $o_db
     */
    public function __construct(DbModel $o_db)
    {
        $this->setupProperties($o_db, 'field_type');
    }

    ### CRUD methods required by interface ###

    /**
     * Creates a record in the field table
     * @param array $a_values Values to insert.
     * @return mixed returns the new id or false on failure
     */
    public function create(array $a_values = [])
    {
        $meth = __METHOD__ . '.';
        if ($a_values == []) {
            $this->logIt("The array must have valid keys and values.", LOG_ALWAYS, $meth . __LINE__);
            return false;
        }
        $a_values = $this->o_db->prepareKeys($a_values);
        $a_required_keys = [
            ':ft_name',
            ':ft_type'
        ];
        if (!Arrays::hasRequiredKeys($a_values, $a_required_keys)) {
            $this->logIt("The array must have both ft_name and ftp_type.", LOG_ALWAYS, $meth . __LINE__);
            return false;
        }
        $a_allowed_keys = $this->prepareListArray($this->a_db_fields);
        $set = $this->buildSqlInsert($a_values, $a_allowed_keys);
        $sql =<<<EOT
INSERT INTO {$this->db_table} (
{$set}
)

EOT;
        $this->logIt("SQL: " . $sql, LOG_OFF, $meth . __LINE__);
        $a_psql = [
            'table_name'  => $this->db_table,
            'column_name' => 'ft_id'
        ];
        if ($this->o_db->insert($sql, $a_values, $a_psql)) {
            $a_new_ids = $this->o_db->getNewIds();
            return $a_new_ids[0];
        }
        $this->error_message = $this->o_db->retrieveFormatedSqlErrorMessage();
        return false;
    }

    /**
     * Reads one or more records from the ritc_field table.
     * @param array $a_values     array of field=>value to search
     * @param array $a_parameters \ref searchparams \ref readparams
     * @return array record(s)
     */
    public function read(array $a_values = [], array $a_parameters = [])
    {
        $a_params = [
            'table_name'   => $this->db_table,
            'a_fields'     => $this->a_db_fields,
            'a_search_for' => $a_values,
            'order_by'     => 'ft_type ASC'
        ];
        $a_parameters = array_merge($a_params, $a_parameters);
        return $this->genericRead($a_parameters);
    }

    /**
     * Updates one or more records in ritc_field
     * @param array $a_values
     * @return bool success or failure
     */
    public function update(array $a_values = [])
    {
    }

    /**
     * Deletes one or more records from ritc_field
     * @param int $field_id
     * @return bool success or failure
     */
    public function delete($field_id = -1)
    {
    }

    ### Other CRUD Methods ###
    /**
     * Creates a record in the ritc_field_option table
     * @param array $a_values
     * @return mixed new id or false on failure
     */
    public function createFieldOption(array $a_values = [])
    {
    }

    /**
     * Creates a new record in the ritc_field_type tables
     * @param array $a_values
     * @return mixed new id or false on failure
     */
    public function createFieldType(array $a_values = [])
    {
    }

    /**
     * Reads one or more records from ritc_field_option table
     * @param array $a_values
     * @return array record(s)
     */
    public function readFieldOption(array $a_values = [])
    {
    }

    /**
     * Returns the record of the field specified by name
     * @param string $field_name field_name in {$this->db_prefix}field is a unique indexed field
     * @return mixed array or bool (false)
     */
    public function readFieldByName($field_name = '')
    {
        $sql = "SELECT * FROM {$this->db_prefix}field WHERE field_name LIKE :field_name";
        $a_search_values = array(':field_name' => $field_name);
        $a_values = $this->o_db->search($sql, $a_search_values);
        if (count($a_values) > 0) {
            return $a_values[0];
        }
        return false;
    }

    /**
     * Read one or more records from {$this->db_prefix}field_type table
     * @param array $a_values
     * @return array record(s)
     */
    public function readFieldType(array $a_values = [])
    {
    }

    /**
     * Returns the custom field info for a section.
     * @param int $section_id
     * @return array|bool $a_fields
     */
    public function readCustomFieldBySection($section_id = 1)
    {
        $sql = "
            SELECT f.field_id, f.field_type_id, f.field_name
            FROM {$this->db_prefix}field as f, {$this->db_prefix}field_section as fs
            WHERE fs.fc_field_id = f.field_id
            AND fs.fc_sec_id = {$section_id}
            AND f.field_enabled = 1
        ";
        $a_values = $this->o_db->search($sql);
        return $a_values;
    }

    /**
     * Updates one or more records in {$this->db_prefix}field_option
     * @param array $a_values
     * @return bool success or failure
     */
    public function updateFieldOption(array $a_values = [])
    {
        if (count($a_values) == 0) {
            return false;
        }
        return false;
    }

    /**
     * Updates one or more records in {$this->db_prefix}field_type
     * @param array $a_values
     * @return bool success or failure
     */
    public function updateFieldType(array $a_values = [])
    {
        if (count($a_values) == 0) {
            return false;
        }
        return false;
    }

    /**
     * Deletes one or more records from {$this->db_prefix}field_option
     * @param array $a_values array of field ids
     * @return bool success or failure
     */
    public function deleteFieldType(array $a_values = [])
    {
        if (count($a_values) == 0) {
            return false;
        }
        return false;
    }

    /**
     * Deletes one or more records from {$this->db_prefix}field_option
     * @param array $a_values array of field ids
     * @return bool success or failure
     */
    public function deleteFieldOption(array $a_values = [])
    {
        if (count($a_values) == 0) {
            return false;
        }
        return false;
    }

}
