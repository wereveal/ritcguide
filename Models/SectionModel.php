<?php
/**
 * @brief     Handles all the database needs (CRUD) for the Sections
 * @ingroup   guide_models
 * @file      SectionModel.php
 * @namespace Ritc\Guide\Models
 * @author    William E Reveal <bill@revealitconsulting.com>
 * @version   1.0.0-alpha.2
 * @date      2016-04-20 07:53:57
 * @note <b>Change Log</b>
 * - v1.0.0-alpha.2 - Add and uses DbUtilityTraits                                    - 2016-04-20 wer
 * - v1.0.0-alpha.1 - first working version after rewrite                             - 2016-03-05 wer
 * - v1.0.0-alpha.0 - initial rewrite                                                 - 12/20/2015 wer
 * - v0.2.0         - testing phase for the Read Methods
 * - v0.1.0         - initial version 03/29/2013
 */
namespace Ritc\Guide\Models;

use Ritc\Library\Helper\Arrays;
use Ritc\Library\Interfaces\ModelInterface;
use Ritc\Library\Services\DbModel;
use Ritc\Library\Traits\DbUtilityTraits;
use Ritc\Library\Traits\LogitTraits;

/**
 * Class SectionModel
 * @class   SectionModel
 * @package Ritc\Guide\Models
 */
class SectionModel implements ModelInterface
{
    use LogitTraits, DbUtilityTraits;

    /**
     * SectionModel constructor.
     * @param \Ritc\Library\Services\DbModel $o_db
     */
    public function __construct(DbModel $o_db)
    {
        $this->setupProperties($o_db, 'section');
    }

    ### CRUD methods required by Interface ###
    /**
     * Creates a new record in the {$db_prefix}section table
     * @param array $a_values
     * @return int|bool
    **/
    public function create(array $a_values = [])
    {
        $a_parameters = [
            'a_required_keys' => [
                'sec_name'
            ],
            'a_field_names'   => $this->a_db_fields,
            'a_psql'          => [
                'table_name'  => $this->db_table,
                'column_name' => $this->primary_index_name
            ]
        ];
        return $this->genericCreate($a_values, $a_parameters);
    }

    /**
     * Returns one or more records from the {$db_prefix}section table
     * @param array $a_search_for        optional, assoc array field_name=>field_value
     * @param array $a_search_parameters optional allows one to specify various settings
     * @ref searchparams for more info on a_search_parameters.
     * @return array $a_records
    **/
    public function read(array $a_search_for = [], array $a_search_parameters = [])
    {
        $a_parameters = [
            'table_name'   => $this->db_table,
            'a_search_for' => $a_search_for,
            'a_fields'     => $this->a_db_fields,
            'order_by'     => 'sec_order'
        ];
        $a_parameters = array_merge($a_parameters, $a_search_parameters);
        return $this->genericRead($a_parameters);
    }

    /**
     * Updates a record in the {$db_prefix}section table
     * @param array $a_query_values
     * @return bool success or failure
    **/
    public function update(array $a_query_values = [])
    {
        return $this->genericUpdate($a_query_values);
    }

    /**
     * Deletes the record specified by id
     * @param int $record_id required
     * @return bool success or failure
     **/
    public function delete($record_id = -1)
    {
        if($record_id == -1 || !is_numeric($record_id)) {
            return false;
        }
        $o_sec_cat = new SecCatModel($o_db);
        $a_results = $o_sec_cat->readMapBySecId($record_id);
        if ($a_results !== false && count($a_results) > 0) {
            $this->error_message = "There are categories still assigned to this section.";
            return false; // a category still exists in the section
        }
        return $this->genericDelete($record_id);
    }

    ### Read Methods ###
    /**
     * Returns the default section.
     * It should be noted that this assumes only one default section.
     * @return bool
     */
    public function readDefaultSection()
    {
        $a_search_for = [':sec_default' => 1];
        return $this->read($a_search_for);
    }

    /**
     * Returns a record from the {$db_prefix}section table
     * @param int $record_id required
     * @return bool|array $a_record
    **/
    public function readSectionById($record_id = -1)
    {
        if ($record_id == -1 || !is_numeric($record_id)) {
            return false;
        }
        $a_search_for = [':sec_id' => $record_id];
        return $this->read($a_search_for);
    }

}
