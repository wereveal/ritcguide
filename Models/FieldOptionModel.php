<?php
/**
 * @brief     Handles the CRUD for field_option table.
 * @ingroup   guide_models
 * @file      FieldOptionModel.php
 * @namespace Ritc\Guide\Models
 * @author    William E Reveal <bill@revealitconsulting.com>
 * @version   1.0.0-alpha.0+3
 * @date      2016-04-01 09:28:27
 * @note Change Log
 * - v1.0.0-alpha.0 - Initial version        - 2016-03-23 wer
 */
namespace Ritc\Guide\Models;

use Ritc\Library\Interfaces\ModelInterface;
use Ritc\Library\Services\DbModel;
use Ritc\Library\Traits\DbUtilityTraits;
use Ritc\Library\Traits\LogitTraits;

/**
 * Class FieldOptionModel.
 * @class   FieldOptionModel
 * @package Ritc\Guide\Models
 */
class FieldOptionModel implements ModelInterface
{
    use LogitTraits, DbUtilityTraits;

    /**
     * FieldOptionModel constructor.
     * @param \Ritc\Library\Services\DbModel $o_db
     */
    public function __construct(DbModel $o_db)
    {
        $this->setupProperties($o_db, 'field_option');
    }

    /**
     * Create a record using the values provided.
     * @param array $a_values
     * @return bool|int
     */
    public function create(array $a_values)
    {
        $a_required_keys = [
            'fo_field_id',
            'fo_field_option'
        ];
        $a_psql = [
            'table_name'  => $this->db_table,
            'column_name' => 'fo_id'
        ];
        $a_parameters = [
            'a_required_keys' => $a_required_keys,
            'a_field_names'   => $this->a_db_fields,
            'a_psql'          => $a_psql
        ];
        return $this->genericCreate($a_values, $a_parameters);
    }

    /**
     * Returns an array of records based on the search params provided.
     * @param array $a_search_for
     * @param array $a_search_parameters
     * @return array|bool
     */
    public function read(array $a_search_for = [], array $a_search_parameters = [])
    {
        $a_parameters = $a_search_parameters == []
            ? ['order_by' => 'fo_field_id ASC, fo_field_option ASC']
            : $a_search_parameters;
        $a_parameters['search_for'] = $a_search_for;
        return $this->genericRead($a_parameters);
    }

    /**
     * Update a record using the values provided.
     * @param array $a_values
     * @return bool
     */
    public function update(array $a_values)
    {
        return $this->genericUpdate($a_values);
    }

    /**
     * Deletes a record based on the id provided.
     * @param int $record_id
     * @return bool
     */
    public function delete($record_id = -1)
    {
        if ($record_id == -1 || !is_numeric($record_id)) {
            $this->error_message = 'The record id provided is invalid';
            return false;
        }
        return $this->genericDelete($record_id);
    }
}
