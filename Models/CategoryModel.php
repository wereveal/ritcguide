<?php
/**
 * @brief     Handles all the database needs (CRUD) for the Category
 * @ingroup   guide_models
 * @file      CategoryModel.php
 * @namespace Ritc\Guide\Models
 * @author    William E Reveal <bill@revealitconsulting.com>
 * @version   1.0.0-alpha.1+2
 * @date      2016-04-01 09:27:26
 * @note <b>Change Log</b>
 * - v1.0.0-alpha.1 - refactoring reflected here.                                  - 2016-03-19 wer
 * - v1.0.0-alpha.0 - start of rewrite                                             - 12/20/2015 wer
 * - v0.1.0 - initial version
 * @todo a lot of changes in the Library database need to be reflected here.
 */
namespace Ritc\Guide\Models;

use Ritc\Library\Services\DbModel;
use Ritc\Library\Traits\DbUtilityTraits;
use Ritc\Library\Traits\LogitTraits;
use Ritc\Library\Interfaces\ModelInterface;

/**
 * Class CategoryModel
 * @class     CategoryModel
 * @package Ritc\Guide\Models
 */
class CategoryModel implements ModelInterface
{
    use LogitTraits, DbUtilityTraits;

    /**
     * Stuff to do when the object is created
     * @param \Ritc\Library\Services\DbModel $o_db
     */
    public function __construct(DbModel $o_db)
    {
        $this->setupProperties($o_db, 'category');
    }

    ### CRUD Required by Interface ###
    /**
     * Adds a new record to the category table
     * @param array $a_cat_values Required defaults to empty
     * @return array|bool new category id(s) or false
     */
    public function create(array $a_cat_values = [])
    {
        if ($a_cat_values == []) {
            return false;
        }
        $a_required_keys = [
            'cat_name'
        ];
        $a_psql = [
            'table_name'  => $this->db_table,
            'column_name' => 'cat_id'
        ];
        $a_parameters = [
            'a_required_keys' => $a_required_keys,
            'a_field_names'   => $this->a_db_fields,
            'a_psql'          => $a_psql
        ];
        return $this->genericCreate($a_cat_values, $a_parameters);
    }

    /**
     * Gets the data from category table
     * @param array $a_search_for optional, an assoc array of field=>value pairs.
     * @note regarding $a_search_for
     * - fields must be valid fields for the category table
     * - if not specified, returns all categories
     * @param array $a_search_parameters optional allows one to specify various settings
     * @note Not all parameters need to be in the array
     * - if doesn't exist, the default setting will be used.
     * - For More information
     *    - \ref searchparams for $a_search_parameters
     *    - \ref readparams for the $a_parameters used by DbUtilityTraits::genericRead()
     * @return bool|array array of records or false
     */
    public function read(array $a_search_for = array(), array $a_search_parameters = array())
    {
        $meth = __METHOD__ . '.';
        $a_search_parameters = $a_search_parameters == []
            ? ['order_by' => 'cat_order ASC']
            : $a_search_parameters;

        if (isset($a_search_parameters['order_by']) === false) {
            $a_search_parameters['order_by'] = 'cat_order ASC';
        }
        $message = "a_search_for:\n" . var_export($a_search_for, TRUE);
        $this->logIt($message, LOG_OFF, $meth . __LINE__);

        $a_search_parameters['a_search_for'] = $a_search_for;
        $a_search_parameters['table_name'] = $this->db_table;
        return $this->genericRead($a_search_parameters);
    }

    /**
     * Updates the category table
     * @param array $a_cat_values
     * @return bool success or failure
     */
    public function update(array $a_cat_values = array())
    {
        if ($a_cat_values == array()) {
            return false;
        }
        return $this->genericUpdate($a_cat_values);
    }

    /**
     * @param int $id
     * @return bool
     */
    public function delete($id = -1)
    {
        if ($id == -1) {
            return false;
        }
        // TODO Need to check if there are entries in this category
        // TODO Need to check for the category_section_map records
        // TODO NEED TO check for cat to cat map records
        return $this->genericDelete($id);
    }

    ### READ methods ###
    /**
     * Returns the categories for a particular section
     * @param int $sec_id optional if not specified returns all categories for all sections sorted by section
     * @return array $a_categories
     */
    public function readCatBySec($sec_id = -1)
    {
        $where = '';
        $a_search_values = '';
        if ($sec_id != -1) { // build the where for the section
            $where .= "AND sec.sec_id = :sec_id \n";
            $a_search_values = array(':sec_id' => $sec_id);
        }
        $sql =<<<SQL
SELECT sec.*, cat.*
FROM {$this->db_prefix}section AS sec,
     {$this->db_table} AS cat,
     {$this->db_prefix}section_category as sc
WHERE sc.sc_sec_id = sec.sec_id
AND sc.sc_cat_id = cat.cat_id
SQL;
        $order_by = "ORDER BY sec.sec_order ASC, cat.cat_order ASC";
        $total_sql = $sql . $where . $order_by;
        return $this->o_db->search($total_sql, $a_search_values);
    }

    /**
     * Returns the default category.
     * @return int|bool
     */
    public function readDefaultCategory()
    {
        $sql = "
            SELECT cat_id
            FROM {$this->db_table}
            WHERE cat_default = :cat_default
            ORDER BY cat_id
        ";
        $a_results = $this->o_db->search($sql, [':cat_default' => 1]);
        if ($a_results != false && count($a_results) > 0) {
            return $a_results[0]['cat_id'];
        }
        return false;
    }

    /**
     * Returns the id of the first category.
     * First category is based on cat_order field in category table
     * @param none
     * @return array first record from the results (should only be one anyway)
     */
    public function readFirstCategory()
    {
        $sql = "
            SELECT cat_id, cat_name, cat_order
            FROM {$this->db_table}
            WHERE cat_active = 1
            ORDER BY cat_order ASC
            LIMIT 1
        ";
        $results = $this->o_db->search($sql);
        if ($results !== false && count($results) > 0) {
            return $results[0];
        }
        return false;
    }

    ### Utilities ###
    /**
     * Sets the required keys for the category table
     * @param array $a_values required
     * @param string $new_or_update
     * @return array $a_values
     */
    public function setRequiredCatKeys(array $a_values = [], $new_or_update = 'new')
    {
        $a_required_keys = array(
            'cat_id',
            'cat_name',
            'cat_description',
            'cat_image',
            'cat_order',
            'cat_active',
            'cat_default'
        );
        $a_values = $this->removeBadKeys($a_required_keys, $a_values);
        $a_missing_keys = $this->o_db->findMissingKeys($a_required_keys, $a_values);
        foreach ($a_missing_keys as $key) {
            switch ($key) {
                case 'cat_id':
                    if ($new_or_update == 'update') { // update requires a valid id
                        return false;
                    }
                    break;
                case 'cat_name':
                    if ($new_or_update == 'new') {
                        return false;
                    }
                    break;
                case 'cat_description':
                    if ($new_or_update == 'new') {
                        $a_values[':cat_description'] = '';
                    }
                    break;
                case 'cat_image':
                    if ($new_or_update == 'new') {
                        $a_values[':image'] = '';
                    }
                    break;
                case 'cat_order':
                    if ($new_or_update == 'new') {
                        $a_values[':cat_order'] = 0;
                    }
                    break;
                case 'cat_active':
                    if ($new_or_update == 'new') {
                        $a_values[':cat_active'] = 1;
                    }
                    break;
                case 'cat_default':
                    if ($new_or_update == 'new') {
                        $a_values[':cat_default'] = 0;
                    }
                    break;
                default:
                    return false;
            }
        }
        return $a_values;
    }
}
