<?php
/**
 * @brief     Does the CRUD for the item_data table.
 * @ingroup   guide_models
 * @file      ItemDataModel.php
 * @namespace Ritc\Guide\Models
 * @author    William E Reveal <bill@revealitconsulting.com>
 * @version   1.0.0-alpha.0+2
 * @date      2016-04-01 09:29:04
 * @note Change Log
 * - v1.0.0-alpha.0 - Initial version        - 2016-03-24 wer
 */
namespace Ritc\Guide\Models;

use Ritc\Library\Interfaces\ModelInterface;
use Ritc\Library\Services\DbModel;
use Ritc\Library\Traits\DbUtilityTraits;
use Ritc\Library\Traits\LogitTraits;

/**
 * Class ItemDataModel.
 * @class   ItemDataModel
 * @package Ritc\Guide\Models
 */
class ItemDataModel implements ModelInterface
{
    use LogitTraits, DbUtilityTraits;

    /**
     * ItemDataModel constructor.
     * @param \Ritc\Library\Services\DbModel $o_db
     */
    public function __construct(DbModel $o_db)
    {
        $this->setupProperties($o_db, 'item_data');
    }

    /**
     * Creates a record.
     * @param array $a_values
     * @return array|bool
     */
    public function create(array $a_values)
    {
        $meth = __METHOD__ . '.';
        $a_parameters = [
            'a_required_keys' => [
                'data_item_id',
                'data_field_id',
                'data_text'
            ],
            'a_field_names'   => $this->a_db_fields,
            'a_psql'          => [
                'table_name'  => $this->db_table,
                'column_name' => 'data_id'
            ],
        ];

        $log_message = 'Create Item Data Values ' . var_export($a_values, TRUE);
        $this->logIt($log_message, LOG_OFF, $meth . __LINE__);
        $log_message = 'Create Parameters ' . var_export($a_parameters, TRUE);
        $this->logIt($log_message, LOG_OFF, $meth . __LINE__);

        return $this->genericCreate($a_values, $a_parameters);
    }

    /**
     * Returns records bases on parameters set.
     * @param array $a_search_for
     * @param array $a_search_params
     * @return array|bool
     */
    public function read(array $a_search_for = [], array $a_search_params = [])
    {
        $a_search_params['a_search_for'] = $a_search_for;
        if (!isset($a_search_params['table_name'])) {
            $a_search_params['table_name'] = $this->db_table;
        }
        if (!isset($a_search_params['a_fields'])) {
            $a_search_params['a_fields'] = $this->a_db_fields;
        }
        if (!isset($a_search_params['order_by'])) {
            $a_search_params['order_by'] = 'data_item_id ASC, data_field_id ASC';
        }
        return $this->genericRead($a_search_params);
    }

    /**
     * Updates record(s) based on values given.
     * @param array $a_values
     * @return bool
     */
    public function update(array $a_values)
    {
        return $this->genericUpdate($a_values);
    }

    /**
     * Deletes a record based on primary index.
     * @param int $id
     * @return bool
     */
    public function delete($id = -1)
    {
        return $this->genericDelete($id);
    }
}