<?php
/**
 * @brief     Handles all the database needs (CRUD) for the Items
 * @ingroup   guide_models
 * @file      ItemModel.php
 * @namespace Ritc\Guide\Models
 * @author    William E Reveal <bill@revealitconsulting.com>
 * @version   1.0.0-alpha.3
 * @date      2016-04-01 09:29:26
 * @note <b>Change Log</b>
 * - v1.0.0-alpha.3 - removed and refactored several methods that were duplicates         - 2016-04-20 wer
 * - v1.0.0-alpha.2 - updated to use DbUtilityTraits for 4 main CRUD methods              - 2016-03-23 wer
 * - v1.0.0-alpha.1 - refactoring elsewhere reflected here                                - 2016-03-19 wer
 * - v1.0.0-alpha.0 - starting over                                                       - 12/20/2015 wer
 * - v0.2.0 - coming back to wg                                                           - 03/20/2014 wer
 *            multiple changes needed due to changes to the Library
 * - v0.1.0 - initial version
 * @todo - Ritc/Guide/Models/ItemModel.php - some methods need to be moved out of here and into appropriate other models.
 */
namespace Ritc\Guide\Models;

use Ritc\Library\Interfaces\ModelInterface;
use Ritc\Library\Services\DbModel;
use Ritc\Library\Traits\DbUtilityTraits;
use Ritc\Library\Traits\LogitTraits;

/**
 * Class ItemModel
 * @class     ItemModel
 * @package Ritc\Guide\Models
 */
class ItemModel implements ModelInterface
{
    use LogitTraits, DbUtilityTraits;

    /** @var \Ritc\Guide\Models\CategoryModel  */
    private $o_cat;
    /** @var \Ritc\Guide\Models\FieldModel  */
    private $o_field;

    /**
     * ItemModel constructor.
     * @param \Ritc\Library\Services\DbModel $o_db
     */
    public function __construct(DbModel $o_db)
    {
        $this->setupProperties($o_db, 'item');
        $this->o_field = new FieldModel($o_db);
        $this->o_cat   = new CategoryModel($o_db);
    }

    ### Interface Methods ###
    /**
     * Creates a new item record
     * @param array $a_values required defaults to empty
     * @return mixed int $new_item_id or bool false
    **/
    public function create(array $a_values = [])
    {
        $meth = __METHOD__ . '.';
        if ($a_values == []) {
            return false;
        }
        $a_parameters = [
            'a_required_keys' => ['item_name'],
            'a_field_names'   => $this->a_db_fields,
            'a_psql'          => [
                'table_name'  => $this->db_table,
                'column_name' => ''
            ],
        ];
        $log_message = 'Parameters for create:  ' . var_export($a_parameters, TRUE);
        $this->logIt($log_message, LOG_OFF, $meth . __LINE__);

        return $this->genericCreate($a_values, $a_parameters);
    }

    /**
     * Generic Read, returns one or more records from the item table
     * @param array $a_search_for        optional, field=>value to search upon
     * @param array $a_search_parameters optional allows one to specify various settings
     * @ref searchparams See for what is allowed.
     * @return array $a_records an array of array(s)
    **/
    public function read(array $a_search_for = [], array $a_search_parameters = [])
    {
        $a_search_parameters['a_search_for'] = $a_search_for;
        if (!isset($a_search_parameters['table_name'])) {
            $a_search_parameters['table_name'] = $this->db_table;
        }
        if (!isset($a_search_parametersp['a_fields'])) {
            $a_search_parameters['a_fields'] = $this->a_db_fields;
        }
        if (!isset($a_search_parameters['order_by'])) {
            $a_search_parameters['order_by'] = 'item_name ASC';
        }
        return $this->genericRead($a_search_parameters);
    }

    /**
     * Updates a record in the item table
     * @param array $a_values all the values to be saved and the item id
     * @return bool true or false
    **/
    public function update(array $a_values = [])
    {
        if ($a_values == []) {
            return false;
        }
        return $this->genericUpdate($a_values);
    }

    /**
     * Deletes an item record from item
     * @param int $item_id
     * @return bool success or failure
    **/
    public function delete($item_id = -1)
    {
        if ($item_id < 1) {
            return false;
        }
        $o_ci = new CatItemMapModel($this->o_db);
        $this->o_db->startTransaction();
        $results = $o_ci->deleteByItem($item_id);
        if (!$results) {
            $this->o_db->rollbackTransaction();
            return false;
        }
        $results = $this->genericDelete($item_id);
        if (!$results) {
            $this->o_db->rollbackTransaction();
            return false;
        }
        else {
            return $this->o_db->commitTransaction();
        }
    }

    ### CREATE methods ###
    /**
     * Adds a new category item record to the category_item table.
     * The connector between the item and its parent category
     * @param array $a_values
     * @return int $record_id ID of th new record
    **/
    public function createCategoryItem(array $a_values = array())
    {
        if ($a_values == array()) { return false; }
        $a_required_keys = array(
            'ci_category_id',
            'ci_item_id',
            'ci_order'
        );
        $a_values = $this->removeBadKeys($a_required_keys, $a_values);
        $a_missing_keys = $this->o_db->findMissingKeys($a_required_keys, $a_values);
        foreach($a_missing_keys as $key) {
            switch ($key) {
                case 'ci_category_id':
                    return false;
                case 'ci_item_id':
                    return false;
                case 'ci_order':
                    $a_values['ci_order'] = 0;
                    break;
            }
        }
        $sql = "
            INSERT INTO {$this->db_prefix}category_item (
                ci_category_id,
                ci_item_id,
                ci_order
            ) VALUES (
                :ci_category_id,
                :ci_item_id,
                :ci_order
            )
        ";
        $results = $this->o_db->insert($sql, $a_values, "{$this->db_prefix}category_item");
        if ($results === false) {
            return false;
        }
        $a_ids = $this->o_db->getNewIds();
        return $a_ids[0];
    }

    /**
     * Creates an item data record in the item_data table
     * @param array $a_values
     * @return bool success or failure
    **/
    public function createItemData(array $a_values = array())
    {
        if ($a_values == array()) { return false; }
        $a_required_keys = array('data_field_id', 'data_item_id', 'data_text');
        if (count($this->findMissingKeys($a_required_keys, $a_values)) > 0) {
            return false;
        }
        if (count($this->findMissingValues($a_required_keys, $a_values)) > 0) {
            return false;
        }
        $a_values = $this->requiredItemDataKeys($a_values, 'new');
        if ($a_values === false) { return false; }
        $sql = "
            INSERT INTO {$this->db_prefix}item_data (
                data_field_id,
                data_item_id,
                data_text,
                data_created_on,
                data_updated_on
            ) VALUES (
                :data_field_id,
                :data_item_id,
                :data_text,
                :data_created_on,
                :data_updated_on
            )
        ";
        $results = $this->o_db->insert($sql, $a_values, "{$this->db_prefix}item_data");
        if ($results === false) {
            return false;
        }
        $a_ids = $this->o_db->getNewIds();
        return $a_ids[0];
    }

    ### READ methods ###
    /**
     * Returns the values for the field
     * @param int $field_id
     * @return mixed array $field_values or bool false
    **/
    public function readFieldById($field_id = -1)
    {
        if ($field_id < 1) { return false; }
        $sql = "
            SELECT *
            FROM {$this->db_prefix}field
            WHERE field_id = :field_id
        ";
        $results = $this->o_db->search($sql, array(':field_id' => $field_id));
        if (count($results) > 0) {
            return $results[0];
        } else {
            return false;
        }
    }

    /**
     * Retuns the values for the field
     * @param string $field_name
     * @return mixed array field values or false
    **/
    public function readFieldByName($field_name = '')
    {
        if ($field_name == '') {
            return false;
        }
        $sql = "SELECT * FROM {$this->db_prefix}field WHERE field_name = :field_name";
        $results = $this->o_db->search($sql, array(':field_name' => $field_name));
        if (count($results) > 0) {
            return $results[0];
        } else {
            return false;
        }
    }

    /**
     * Returns the values for the field
     * @param int $old_field_id
     * @return mixed array $field_values or bool false
    **/
    public function readFieldByOldId($old_field_id = -1)
    {
        if ($old_field_id < 1) { return false; }
        $sql = "
            SELECT *
            FROM {$this->db_prefix}field
            WHERE field_old_field_id = :field_old_field_id
        ";
        $results = $this->o_db->search($sql, array(':field_old_field_id' => $old_field_id));
        if (count($results) > 0) {
            return $results[0];
        } else {
            return false;
        }
    }

    /**
     * Returns the records from items from a category.
     * @param int $cat_id required, if '' return array()
     * @param array $a_search_params optional
     * @return array $a_items
    **/
    public function readItemByCategory($cat_id = -1, array $a_search_params = array())
    {
        if ($cat_id < 1) {
            return array();
        }
        $sql_where = $this->sqlWhere($a_search_params);
        $sql = "
            SELECT i.*
            FROM {$this->db_prefix}item as i, {$this->db_prefix}category_item as ci
            WHERE i.item_id = ci.ci_item_id
            AND ci.ci_category_id = :ci_category_id
            {$sql_where}
        ";
        $this->logIt($sql, LOG_OFF, __METHOD__ . '.' . __LINE__);

        return $this->o_db->search($sql, array(':ci_category_id' => $cat_id));
    }

    /**
     * Returns the records from item.
     * A shortcut which uses the main method readItem()
     * @param string $item_name
     * @return array $a_items
    **/
    public function readItemByName($item_name = '')
    {
        return $this->read(array(':item_name' => $item_name), array('comparison_type' => 'LIKE', 'limit_to' => 1));
    }

    /**
     * Returns the records that match the first letter of the item name with the param
     * @param string $the_letter required
     * @param int $start which record the results should start from
     * @param int $limit_to optional limit the number of records found
     * @return array $a_records
    **/
    public function readItemByNameFirstLetter($the_letter = 'A', $start = 0, $limit_to = 0)
    {
        $the_letter = substr(trim($the_letter), 0, 1);
        if ($the_letter == '') {
            return false;
        }
        $a_search_pairs = array(':item_name' => $the_letter . '%');
        $limit_to = $limit_to == 0 ? '' : (int) $limit_to;
        $a_search_parameters = array(
            'comparison_type' => 'LIKE',
            'starting_from'   => $start,
            'limit_to'        => $limit_to,
            'order_by'        => 'item_name'
        );
        return $this->read($a_search_pairs, $a_search_parameters);
    }

    /**
     * Returns the record for the Item specified by item_old_id
     * @param mixed int|string $old_item_id
     * @return mixed array or false
    **/
    public function readItemByOldItemId($old_item_id = '')
    {
        if ($old_item_id == '') {
            return false;
        }
        $a_search_values = array('item_old_id' => $old_item_id);
        return $this->read($a_search_values);
    }

    /**
     * Returns items from specified section.
     * This normally is used to return either a set number of random or featured
     * items from a specific section. However, search params can change what item records
     * from a section will be returned.
     * @param int $section_id required, if '' return array()
     * @param array $a_search_params optional, defaults as follows
     *     array(
     *         'is_active' => true,
     *         'is_random' => true, // used if is_featured is false
     *         'is_featured' => true, // affects the use of is_random
     *         'limit_to' => 10, // limit the number of records to return
     *         'order_by' => 'i.item_name' // not used if is_random === true
     *     )
     *     Not all parameters need to be in the array, if doesn't exist, the default setting will be used.
     * @return array $a_items
    **/
    public function readItemBySection($section_id = -1, array $a_search_params = array())
    {
        if ($section_id < 1) {
            return array();
        }
        $sql_where = $this->sqlWhere($a_search_params);
        $sql = "
            SELECT i.*
            FROM {$this->db_prefix}item as i, {$this->db_prefix}category_item as ci, {$this->db_prefix}section_category as sc
            WHERE i.item_id = ci.ci_item_id
            AND ci.ci_category_id = sc.sc_cat_id
            AND sc.sc_sec_id = :sc_sec_id
            {$sql_where}
        ";
        $a_search_pairs = array(":sc_sec_id" => $section_id);
        return $this->o_db->search($sql, $a_search_pairs);
    }

    /**
     * Gets the number of records in items that match the parameters.
     * @param array $a_search_pairs
     * @param array $a_search_parameters
     * @return int record count
    **/
    public function readItemCount(array $a_search_pairs = array(), array $a_search_parameters = array())
    {
        if ($a_search_pairs == array()) { return 0; }
        $sql = "SELECT COUNT(*) as count FROM {$this->db_prefix}item ";
        $sql .= $this->buildSqlWhere($a_search_pairs, $a_search_parameters);
        $results = $this->o_db->search($sql, $a_search_pairs);
        return $results[0]['count'];
    }

    /**
     * Returns a list of items which are marked as featured
     * @param int $num_of_records
     * @return array $a_items
    **/
    public function readItemFeatured($num_of_records = 10)
    {
        return $this->read(
            array('item_featured' => '1'),
            array('limit_to' => $num_of_records, 'order_by' => 'item_name')
        );
    }

    /**
     * Returns the ids of the records
     * @param int $num_of_records optional
     * @return array $a_items
    **/
    public function readItemIds($num_of_records = 0)
    {
        $num_of_records = (int) $num_of_records;
        $sql = "
            SELECT item_id
            FROM {$this->db_prefix}item
            ORDER BY item_id
        ";
        if ($num_of_records > 0) {
            $sql .= "LIMIT {$num_of_records}";
        }
        return $this->o_db->search($sql);
    }

    /**
     * Returns a list of random items.
     * @param int $num_of_records
     * @return array $a_items
    **/
    public function readItemRandom($num_of_records = 10)
    {
        $db_type = $this->o_db->getDbType();
        $sql = "
            SELECT item_id, item_name
            FROM {$this->db_prefix}item
            WHERE item_active = 1
        ";
        switch ($db_type) {
            case 'mysql':
                $sql .= "            ORDER BY rand() LIMIT {$num_of_records}";
                break;
            case 'pgsql':
            case 'sqlite':
                $sql .= "            ORDER BY random() LIMIT {$num_of_records}";
                break;
            default:
                $sql .= '';
        }
        $results = $this->o_db->search($sql);
        return $results;
    }

    ### DELETE methods ###
    /**
     * Deletes the bridge record between category and item from category_item table.
     * @param int $cat_id required
     * @param int $item_id required
     * @return bool success or failure
    **/
    public function deleteCatetoryItem($cat_id = -1, $item_id = -1)
    {
        if ($cat_id < 1 || $item_id < 1) { return false; }
        return false;
    }

    ### Utilities ###
    /**
     * Sets the required keys for the item_data table
     * @param array $a_values required
     * @param string $new_or_update optional defaults to new
     * @return mixed array|bool $a_values
    **/
    public function requiredItemDataKeys(array $a_values = array(), $new_or_update = 'new')
    {
        $a_required_keys = array(
            'data_id',
            'data_field_id',
            'data_item_id',
            'data_text',
            'data_created_on',
            'data_updated_on'
        );
        $a_values = $this->removeBadKeys($a_required_keys, $a_values);
        $a_missing_keys = $this->o_db->findMissingKeys($a_required_keys, $a_values);
        $current_timestamp = date('Y-m-d H:i:s');

        foreach ($a_missing_keys as $key) {
            switch ($key) {
                case 'data_id':
                    if ($new_or_update == 'update') {
                        return false;
                    } // else it is a new record so there is no data_id yet.
                    break;
                case 'data_field_id':
                case 'data_item_id':
                case 'data_text':
                    if ($new_or_update == 'new') {
                        return false;
                    }
                    break;
                case 'data_created_on':
                    if ($new_or_update == 'new') {
                        $a_values['data_created_on'] = $current_timestamp;
                    }
                    break;
                case 'data_updated_on':
                    $a_values['data_updated_on'] = $current_timestamp;
                    break;
                default:
                    return false;
            }
        }
        return $a_values;
    }

    /**
     * Creates the sql for the where, order by and limit
     * @param array $a_params
     * @return string $sql
    **/
    public function sqlWhere(array $a_params = array())
    {
        ### Defaults ###
        $is_active    = true;
        $is_random    = false;
        $is_featured  = true;
        $limit_to     = 10;
        $order_by     = 'i.item_name';

        $sql_active   = "
            AND i.item_active = 1
        ";
        $sql_featured = "
            AND i.item_featured = 1
        ";
        $sql_order_by = "
            ORDER BY {$order_by}
        ";
        $sql_limit_to = "
            LIMIT {$limit_to}
        ";
        foreach ($a_params as $key => $value) {
            switch ($key) {
                case 'is_active':
                    $is_active = (bool) $value;
                    break;
                case 'is_random':
                    $is_random = (bool) $value;
                    break;
                case 'is_featured':
                    $is_featured = (bool) $value;
                    break;
                case 'limit_to':
                    $limit_to = (int) $value;
                    if ($limit_to === 0) {
                        $sql_limit_to = "            -- return all records, no limit \n";
                    }
                    else {
                        $sql_limit_to = "            LIMIT {$limit_to} \n";
                    }
                    break;
                case 'order_by':
                    $sql_order_by = "            ORDER BY {$value} \n";
                    break;
                default:
                    // not valid key, skip it
            }
        }
        if ($is_active === false) {
            $sql_active = "-- active and inactive item records \n";
        }
        if ($is_random === true && $is_featured === false) {
            $sql_featured = "            -- using random and not featured items \n";
            $db_type = $this->o_db->getDbType();
            switch ($db_type) {
                case 'mysql':
                    $sql_order_by = "            ORDER BY rand() \n";
                    break;
                case 'pgsql':
                case 'sqlite':
                    $sql_order_by = "            ORDER BY random() \n";
                    break;
                default:
                    $sql_order_by .= "            -- invalid db type, can't set order by random \n";
            }
        }
        return $sql_active . $sql_featured . $sql_order_by . $sql_limit_to;
    }
}
