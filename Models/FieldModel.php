<?php
/**
 * @brief     Handles all the database needs (CRUD) for the Fields
 * @ingroup   guide_models
 * @file      FieldModel.php
 * @namespace Ritc\Guide\Models
 * @author    William E Reveal <bill@revealitconsulting.com>
 * @version   1.0.0-alpha.1
 * @date      2016-03-18 15:48:45
 * @note <b>Change Log</b>
 * - v1.0.0-alpha.2 - bug fix read method needed to match interface                 - 2016-03-24 wer
 * - v1.0.0-alpha.1 - Refactoring of DbModel reflected here.                        - 2016-03-18 wer
 * - v1.0.0-alpha.0 - start of rewrite                                              - 12/20/2015 wer
 * - v0.1.0 - initial version
 * @todo Refactor to use the DbUtiltyTraits
 * @todo test
 */
namespace Ritc\Guide\Models;

use Ritc\Library\Helper\Arrays;
use Ritc\Library\Interfaces\ModelInterface;
use Ritc\Library\Services\DbModel;
use Ritc\Library\Traits\DbUtilityTraits;
use Ritc\Library\Traits\LogitTraits;

/**
 * Class FieldModel
 * @class   FieldModel
 * @package Ritc\Guide\Models
 */
class FieldModel implements ModelInterface
{
    use LogitTraits, DbUtilityTraits;

    /**
     * FieldModel constructor.
     * @param \Ritc\Library\Services\DbModel $o_db
     */
    public function __construct(DbModel $o_db)
    {
        $this->setupProperties($o_db, 'field');
    }

    ### CRUD methods required by interface ###

    /**
     * Creates a record in the field table
     * @param array $a_values
     * @return mixed returns the new id or false on failure
     */
    public function create(array $a_values = [])
    {
        $a_parameters = [
            'a_required_keys' => [
                ':field_type_id',
                ':field_name'
            ],
            'a_psql' => [
                'table_name'  => $this->db_table,
                'column_name' => 'field_id'
            ]
        ];
        return $this->genericCreate($a_values, $a_parameters);
    }

    /**
     * Reads one or more records from the field table
     * @param array $a_search_for
     * @param array $a_search_parameters \ref searchparams \ref readparams
     * @return array record(s)
     */
    public function read(array $a_search_for = [], array $a_search_parameters = [])
    {
        $meth = __METHOD__ . '.';
        $a_search_parameters['a_search_for'] = $a_search_for;
        if (!isset($a_search_parameters['table_name'])) {
            $a_search_parameters['table_name'] = $this->db_table;
        }
        if (!isset($a_search_parametersp['a_fields'])) {
            $a_search_parameters['a_fields'] = $this->a_db_fields;
        }
        if (!isset($a_search_parameters['order_by'])) {
            $a_search_parameters['order_by'] = 'field_name ASC';
        }
        $log_message = 'Read Parameters ' . var_export($a_search_parameters, TRUE);
        $this->logIt($log_message, LOG_OFF, $meth . __LINE__);

        return $this->genericRead($a_search_parameters);
    }

    /**
     * Updates one or more records in field
     * @param array $a_values
     * @return bool success or failure
     */
    public function update(array $a_values = [])
    {
        if ($a_values == []) {
            $this->error_message = "Could not update the record, no values given.";
            return false;
        }
        $a_values = $this->o_db->prepareKeys($a_values);
        $a_allowed_fields = $this->a_field_names;
        $a_allowed_fields = $this->prepareListArray($a_allowed_fields);
        $a_values = Arrays::removeUndesiredPairs($a_values, $a_allowed_fields);
        $set_sql = $this->buildSqlSet($a_values, [':field_id'], $a_allowed_fields);
        $sql = "UPDATE {$this->db_table} SET $set_sql";
        $results = $this->o_db->update($sql, $a_values);
        if ($results === false) {
            $this->error_message = $this->o_db->retrieveFormatedSqlErrorMessage();
        }
        return $results;
    }

    /**
     * Deletes one or more records from field
     * @param int $field_id
     * @return bool success or failure
     */
    public function delete($field_id = -1)
    {
        if ($field_id == -1) {
            $this->error_message = "Field ID missing so could not delete it.";
            return false;
        }
        // TODO: Need to check to see if item_data record(s) exist
        // TODO: Need to check for field sec map records
        // TODO: Need to check for field_option records
        $sql = "DELETE FROM {$this->db_table} WHERE field_id = :field_id";
        $results = $this->o_db->delete($sql, [':field_id' => $field_id]);
        if ($results === false) {
            $this->error_message = $this->o_db->retrieveFormatedSqlErrorMessage();
        }
        return $results;
    }

    ### Other CRUD Methods ###
    /**
     * @param int $field_id
     * @return array|bool
     */
    public function readFieldById($field_id = -1)
    {
        if ($field_id == -1) {
            $this->error_message = "Field ID missing so could not find it.";
            return false;
        }
        $a_search_for = [':field_id' => $field_id];
        return $this->read($a_search_for);
    }

    /**
     * Returns the record of the field specified by name
     * @param string $field_name required, field_name in field is a unique indexed field
     * @return mixed array or bool (false)
     */
    public function readFieldByName($field_name = '')
    {
        if ($field_name == '') {
            $this->error_message = "Field ID missing so could not find it.";
            return false;
        }
        $a_search_for = [':field_name' => $field_name];
        return $this->read($a_search_for);
    }

}
